/*
    Created: RAP - March 2017 for May Release
    Purpose: PD-501 - Lien Detail Page - Lien Resolution Fee Tab
*/

({
    doInit : function(component, event, helper) {
        var action = component.get("c.getFees");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var temp = response.getReturnValue();
            component.set("v.fees", temp); 
            component.set("v.numfees", temp.length);
        });
        $A.enqueueAction(action);
    }
})