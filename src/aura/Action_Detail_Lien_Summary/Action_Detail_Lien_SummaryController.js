/*
	Created: RAP - February 2017 for March Release
    Purpose: PD-159 - Action - Lien Summary Tab
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getStageMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            var smMap = response.getReturnValue();
       			var result = [];
	            var stages = [];
	            var temp2 = [];
	            var temp3 = [];
	            for (var lienType in smMap) {
	            	stages = smMap[lienType]; // stages keyset is lien type
	            	var result2 = [];
	            	for (var stage in stages) {
		       			temp2 = stages[stage]; // temp2 keyset is stage
	            		temp3 = temp2['All'];
	       				console.log('stage = ' + stage);
	       				console.log('temp3[0] = ' + temp3[0]);
		       			result2.push({
		       				key: stage,
		       				value: temp3
		       			});
		       		}
	       			result.push({
	       				key: lienType,
	       				value: result2
	       			});
		       	}
	            component.set("v.liensByStage", result);
            } else {
                console.log('Problem getting stage metrics, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.getLienStageLabels(component);
        helper.getLienTypeLabels(component);
    }
})