/*  
    Created: lmsw - May 2017
    Purpose: PD-640 Add Claimant totals and Awards totals

*/
({
	getStatusPercent : function(component) {
		var value = component.get("v.statusTotal");
		var percent = 0;
		var action = component.get("c.getClaimantsCountForAction");
		action.setParams({"aId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
            	var total = response.getReturnValue();
            	if(total>0){ percent = Math.round((value/total)*100); } 
				component.set("v.statusPercent", percent);                       
            } else {
                console.log('Problem getting Total Claimants Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	},
	getAwardPercent : function(component) {
		var value = component.get("v.awardTotal");
		var percent = 0;
		var action = component.get("c.getAwardsCountForAction");
		action.setParams({"aId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
            	var total = response.getReturnValue();
            	if(total>0){ percent = Math.round((value/total)*100); } 
				component.set("v.awardPercent", percent);                       
            } else {
                console.log('Problem getting Total Award Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})