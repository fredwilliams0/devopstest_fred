/*  
    Created: RAP - January 2017 for March Release
    Purpose: PD-348 - Display data for just one Action

History:

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-436 -Modify as Parent component

    Updated: lmsw - May 2017
    Purpose: PD-640 Add Claimant totals and Awards totals
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getClaimantStatusCount");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var total = 0;
                var arrayOfMapKeys = [];
                var storeResponse = response.getReturnValue();
                component.set("v.statusCountMap", storeResponse);
                component.set("v.actionID", component.get("v.recordId"))
                for(var singleKey in storeResponse) {
                    arrayOfMapKeys.push(singleKey);
                    total = total + storeResponse[singleKey];
                }
                component.set('v.statusTotal', total);
                helper.getStatusPercent(component);  
                component.set('v.lstKey', arrayOfMapKeys);
            } else {
                console.log('Problem getting Claimant Status Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getDistributionStatusCount");
        action2.setParams({"aId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var total = 0;
                var arrayOfMapKeys = [];
                var storeResponse = response.getReturnValue();
                component.set("v.distributionCountMap", storeResponse);
                component.set("v.actionID", component.get("v.recordId"))
                for(var singleKey in storeResponse) {
                    arrayOfMapKeys.push(singleKey);
                    total = total + storeResponse[singleKey];
                }
                component.set('v.awardTotal', total);
                helper.getAwardPercent(component); 
                component.set('v.lstKeyD', arrayOfMapKeys);
            } else {
                console.log('Problem getting Distribution Status Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);    
    }
    
})