({
	statusCircle : function(component) {
		// Determine color of circle
		var selected = component.find("ClaimantStatusSelect").get("v.value");
		selected = selected.replace(/ /g,"_");
		if (selected == "Cleared") {
			component.set("v.circleStyle", "defaultCircle greenCircle");
		} else if(selected == "Released_to_a_Holdback") {
			component.set("v.circleStyle", "defaultCircle yellowCircle");
		} else {
			component.set("v.circleStyle", "defaultCircle redCircle");
		} 
	}
})