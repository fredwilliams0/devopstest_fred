({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAlerts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            component.set("v.alerts", response.getReturnValue());
            } else {
                console.log('Problem getting alerts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        var action1 = component.get("c.getFilterMap");
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
	            var fMap = response.getReturnValue();
	            var temp;
            	var result = [];
	            for (var pung in fMap) {
	            	temp = fMap[pung];
           			console.log("pung = " + pung);
           			console.log("temp = " + temp);
	       			result.push({
	       				key: pung,
	       				value: temp
	       			});
	       		}
	            component.set("v.filterMap", result);
            } else {
                console.log('Problem getting filters, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);    
    }
})