/* 
    Created: lmsw - January 2017 for first release
    Purpose: PD-399 - Add methods for Conga button Lien page 

History:

    Updated: lmsw - Februrary 2017 for March Release
    Purpose: PD-399 - Peer code review remove commented code
             PRODSUPT-8 - Change name in title

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-80 Fix Medicare_Model -> Medicare_Modeled and remove hiding tab

*/
({
	getLienRecord : function(component,event,helper){
		var action = component.get("c.getLien");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
                //this.hideDateTab(component);
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
	},  
// start PRODSUPT-80
//	removed hideDateTab : function(component,event) {
// end PRODSUPT-80
// start PRODSUPT-8
    getLienTypes : function(component) {
        var action = component.get("c.getLienTypeMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienTypes", result);
               
            } else {
                console.log('Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
// end PRODSUPT-8
})