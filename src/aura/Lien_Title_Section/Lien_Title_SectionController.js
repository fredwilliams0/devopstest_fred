/* 
    Created: Marc Dysart - January 2017 for first release
    Purpose: PD-304 - Lien Title Section Component for Lien 

History:

    Updated: lmsw - January 2017 for March Release
    Purpose: PD-399 - Add Conga button to Lien page 

    Update: lmsw - February 2017 for March Release
    Purpose: PD-399 - Modify Conga Composer Window to recommended size
             PRODSUPT-8 - Change name in title

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 - Modify to use Lien Record Types
    
    Updated: lmsw - April 2017
    Purpose: PD-574 - Modify Documents button for templates
    
    Updated MF - 4/18
    Purpose PD-602 - Add Conga/Box Integration Features

    Updated MF - 5/2
    Purpose PD-672 and PD-673 - Change record id from lien to claimant
    
    
*/
({

    doInit : function(component, event, helper) {
        helper.getLienRecord(component);
// PRODSUPT-8, PRODSUPT-57
        helper.getLienTypes(component);
    },
    editLien : function(component, event, helper) { 
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
          "recordId": component.get("v.lien.Id")
        });
        editRecordEvent.fire();
    },
    generateConga : function(component,event, helper) {
        // Get lien information for template
        var lienId = component.get("v.lien.Id");
        var acctId = component.get("v.lien.Account__c");
        var claimantId = component.get("v.lien.Claimant__c");
        // Get Conga Custom Labels to build link
        var congaLink = $A.get("$Label.c.Conga_baseUrl")+'--apxtconga4.'+$A.get("$Label.c.Conga_sfInstance")+'.visual.force.com/apex/Conga_Composer';
        var recordLink = '?id='+claimantId;  //PD672, 673
// start PD-574
        // Get Conga Queries that are in Custom labels
        var lienholderQuery = $A.get("$Label.c.Conga_Lienholder")+acctId;
        var lien2Query = $A.get("$Label.c.Conga_Lien2")+lienId;
        var injuryQuery = $A.get("$Label.c.Conga_Injury")+lienId;
        var templateId = $A.get("$Label.c.Conga_TemplateId");
// end PD-574
// start PD-602
		//add additional parameter information for box integration
		var boxEnableParams = $A.get("$Label.c.Conga_BoxEnablement");
        var boxRootFolder = $A.get("$Label.c.Conga_BoxRootFolder");
        var boxFolderName = component.get("v.lien.Claimant__r.Name") //folder names in Box are the same as the claimants name
        var boxFolderNameCln = boxFolderName.replace(/ /g, '+'); //box paths require "+" instead of space chars.
// end PD-602
        var congaUrl = 	congaLink+
            		   	recordLink+'&QueryId='+
            	   	   	lienholderQuery+','+
                       	lien2Query+','+
            		   	injuryQuery+'&TemplateId='+
            			templateId+
            			boxEnableParams+  //PD-602
              			boxRootFolder + //PD-602
            			boxFolderNameCln;  //PD-602
// Modify conga composer window
        window.open(congaUrl, '_blank',"height=660px width=900px");
        
        /*  Conga button link 
         *  Update/Create Custom Labels for conga queries, conga Templates, baseURL, and sfInstance
         *  Update for UAT:
          	/apex/APXTConga4__Conga_Composer 
            ?id={!Lien__c.Id} 
            &QueryId=[Lienholder]a0Fn0000002pkwG?pv0={!Lien__c.AccountId__c}, 
            [Lien2]a0Fn0000002pkwL?pv0={!Lien__c.Id}, 
            [Injury]a0Fn0000002pkz0?pv0={!Lien__c.Id} 
            &TemplateId=a0Nn0000000hZXz
            &DS7=600 (optional for automatic processing, left out for now)
			&BoxVisible=1
            &OFP=Salesforce-UAT\Claimants\FolderName  (replace foldername with Box foldername which is the name of the claimant)
			
        */
   }
})