({
    showMedicaid : function(component, event, helper) {
        var cmpMsg = component.find("Medicaid");
        var showButton = component.find("Medicaid_1");
        var hideButton = component.find("Medicaid_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedicaid : function(component,event) {
        var cmpMsg = component.find("Medicaid");
        var showButton = component.find("Medicaid_2");
        var hideButton = component.find("Medicaid_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showMedTrad : function(component, event, helper) {
        var cmpMsg = component.find("MedTrad");
        var showButton = component.find("MedTrad_1");
        var hideButton = component.find("MedTrad_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedTrad : function(component,event) {
        var cmpMsg = component.find("MedTrad");
        var showButton = component.find("MedTrad_2");
        var hideButton = component.find("MedTrad_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showMedMod : function(component, event, helper) {
        var cmpMsg = component.find("MedMod");
        var showButton = component.find("MedMod_1");
        var hideButton = component.find("MedMod_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideMedMod : function(component,event) {
        var cmpMsg = component.find("MedMod");
        var showButton = component.find("MedMod_2");
        var hideButton = component.find("MedMod_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showOtherGovt : function(component, event, helper) {
        var cmpMsg = component.find("OtherGovt");
        var showButton = component.find("OtherGovt_1");
        var hideButton = component.find("OtherGovt_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideOtherGovt : function(component,event) {
        var cmpMsg = component.find("OtherGovt");
        var showButton = component.find("OtherGovt_2");
        var hideButton = component.find("OtherGovt_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showNonP : function(component, event, helper) {
        var cmpMsg = component.find("NonP");
        var showButton = component.find("NonP_1");
        var hideButton = component.find("NonP_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hideNonP : function(component,event) {
        var cmpMsg = component.find("NonP");
        var showButton = component.find("NonP_2");
        var hideButton = component.find("NonP_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showPLRPCS : function(component, event, helper) {
        var cmpMsg = component.find("PLRPCS");
        var showButton = component.find("PLRPCS_1");
        var hideButton = component.find("PLRPCS_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hidePLRPCS : function(component,event) {
        var cmpMsg = component.find("PLRPCS");
        var showButton = component.find("PLRPCS_2");
        var hideButton = component.find("PLRPCS_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    showPLRPMod : function(component, event, helper) {
        var cmpMsg = component.find("PLRPMod");
        var showButton = component.find("PLRPMod_1");
        var hideButton = component.find("PLRPMod_2");
        $A.util.removeClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
    hidePLRPMod : function(component,event) {
        var cmpMsg = component.find("PLRPMod");
        var showButton = component.find("PLRPMod_2");
        var hideButton = component.find("PLRPMod_1");
        $A.util.addClass(cmpMsg, "hide");
        $A.util.removeClass(showButton, "hide");
        $A.util.addClass(hideButton, "hide");
    },
})