({
    doInit : function(component, event, helper) {
        var action = component.get("c.getActions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.actions", response.getReturnValue());
            } else {
                console.log('Problem getting actions, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    },

    SelectAction : function(component, event, helper) { 
    	var urlEvent = $A.get("e.force:navigateToURL");
    	var aId = event.target.name;
    	var action2 = component.get("c.setAction");
    	action2.setParams({"aId": aId});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {} 
            else {
                console.log('Problem setting action, response state: ' + state);
            }
        });     
        $A.enqueueAction(action2);
    	urlEvent.setParams({
    		"isredirect": false,
    		"url": "/" + aId
    	});
    	urlEvent.fire();
    }
})