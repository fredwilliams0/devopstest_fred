<!-- 
	Created: lmsw - January 2017 for March release
    Purpose: PD-400 - Dates Tab Component - Lien

History:

	Updated: lmsw - January 2017 for March Release
    Purpose: PD-170 - Code Review Changes

    Updated: lmsw - March 2017 for March Release
    Purpose: PRODSUPT-80 Fix Medicare_Model -> Medicare_Modeled

    Updated: lmsw - March 2017 for May Release
    Purpose: PRODSUPT-57 - Modify to use Lien Record Types

    Updated: lmsw - May 2017
    Purpose: PD-654 Add dates for record lien type PLRP Claim Detail and PLRP Model
-->
<aura:component controller="Controller_Aura1" access="global" implements="flexipage:availableForAllPageTypes,force:hasRecordId">
	<aura:attribute name="component_description" type="String" default="Component Description" access="global" />
	<aura:attribute name="lienDatesMap" type="map" access="global"/>
	<aura:attribute name="lien" type="Lien__c" default="{'sobjectType':'Lien__c'}"/>
	<aura:handler name="init" value="{!this}" action="{!c.doInit}" />	
<!-- Code Review Changes -->		
	<div aura:id="hasDates" class="slds-hide">	
		<h2 class="slds-section__title slds-align--absolute-center">Eligibility Dates</h2>
<!-- end changes -->
		<div class="slds-grid slds-wrap slds-grid--pull-padded">
			<div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-3">
				<article class="slds-card">
					<div class="slds-card__header slds-grid">
						<header class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-align--absolute-center">Part A</div>
						</header>
					</div>
					<div class="slds-card__body slds-scrollable--x">
						<table class="slds-table slds-table--bordered">
							<thead>
								<tr class="slds-text-heading--label">
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">START</div>
									</th>
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">END</div>
									</th>
								</tr>
							</thead>
							<tbody aura:id="main">
								<aura:iteration items="{!v.lienDatesMap.PartA}" var="lienDateA">
									<tr class="slds-hint-parent">
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="START">
											<ui:outputDate value="{!lienDateA.Date_Start__c}" />
										</td>
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="END">
											<ui:outputDate value="{!lienDateA.Date_End__c}" />
										</td>
									</tr>
								</aura:iteration>
							</tbody>
						</table>
				    </div>
				</article>
			</div>
			<div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-3">
				<article class="slds-card">
					<div class="slds-card__header slds-grid">
						<header class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-align--absolute-center">Part B</div>
						</header>
					</div>
					<div class="slds-card__body slds-scrollable--x">
						<table class="slds-table slds-table--bordered">
							<thead>
								<tr class="slds-text-heading--label">
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">START</div>
									</th>
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">END</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<aura:iteration items="{!v.lienDatesMap.PartB}" var="lienDateB">
									<tr class="slds-hint-parent">
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="START">
											<ui:outputDate value="{!lienDateB.Date_Start__c}" />
										</td>
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="END">
											<ui:outputDate value="{!lienDateB.Date_End__c}" />
										</td>
									</tr>
								</aura:iteration>
							</tbody>
						</table>
					</div>
				</article>
			</div>
			<div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-3">
				<article class="slds-card">
					<div class="slds-card__header slds-grid">
						<header class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-align--absolute-center">Part C</div>
						</header>
					</div>
					<div class="slds-card__body slds-scrollable--x">
						<table class="slds-table slds-table--bordered">
							<thead>
								<tr class="slds-text-heading--label">
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">START</div>
									</th>
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center">END</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<aura:iteration items="{!v.lienDatesMap.PartC}" var="lienDateC">
									<tr class="slds-hint-parent">
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="START">
											<ui:outputDate value="{!lienDateC.Date_Start__c}" />
										</td>
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="END">
											<ui:outputDate value="{!lienDateC.Date_End__c}" />
										</td>
									</tr>
								</aura:iteration>
							</tbody>
						</table>
					</div>
				</article>
			</div>
		</div>
	</div>
	<div aura:id="plrpDates" class="slds-hide">
		<h2 class="slds-section__title slds-align--absolute-center">Eligibility Dates</h2>
		<div class="slds-container--center">
				<article class="slds-card">
					<div class="slds-card__header slds-grid">
						<header class="slds-media slds-media--center slds-has-flexi-truncate">
							<div class="slds-align--absolute-center">Private</div>
						</header>
					</div>
					<div class="slds-card__body slds-scrollable--x">
						<table class="slds-table slds-table--bordered ">
							<thead>
								<tr class="slds-text-heading--label">
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center leftMargin">START</div>
									</th>
									<th class="slds-is-sortable" scope="col">
										<div class="slds-truncate slds-text-align--center rightMargin">END</div>
									</th>
								</tr>
							</thead>
							<tbody>
								<aura:iteration items="{!v.lienDatesMap.Private}" var="lienDateC">
									<tr class="slds-hint-parent">
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="START">
											<div class="leftMargin"><ui:outputDate value="{!lienDateC.Date_Start__c}" /></div>
										</td>
										<td class="slds-truncate slds-text-align--center" scope="row" data-label="END">
											<div class="rightMargin"><ui:outputDate value="{!lienDateC.Date_End__c}" /></div>
										</td>
									</tr>
								</aura:iteration>
							</tbody>
						</table>
					</div>
				</article>
			</div>
	</div>
	<div aura:id="noDates" class="slds-hide">
		<i>There are no Eligibility dates for this type of Lien</i>
	</div>
</aura:component>