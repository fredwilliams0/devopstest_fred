/* 
    Created: lmsw - February 2017 for March Release
    Purpose: PD-348 - Display Award distribution status for an action
*/
({
	statusLabel : function(component) {
		// Get Label of Status picklist
		var selected = component.get("v.key");
		selected = selected.replace(/_/g," ");
		component.set("v.keyLabel", selected);
	},
	getDPercent : function(component) {
		var value = component.get("v.value");
		var percent = 0;
		var action = component.get("c.getAwardsCountForAction");
		action.setParams({"aId": component.get("v.actionID")});
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
            	var total = response.getReturnValue();
            	if(total>0){ percent = Math.round((value/total)*100); } 
				component.set("v.percentD", percent);                      
            } else {
                console.log('Problem getting Total Awards Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})