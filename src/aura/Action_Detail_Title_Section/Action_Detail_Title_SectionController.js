/*  
    Created: RAP - January 2017 for March Release
    Purpose: PD-348 - Display data for just one Action

History:

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-436 -Modify as Parent component

*/
({
    doInit : function(component, event, helper) {
        helper.getActionRecord(component);
    },
    editAction : function(component, event, helper) { 
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
          "recordId": component.get("v.currentAction.Id")
        });
        editRecordEvent.fire();
    }
})