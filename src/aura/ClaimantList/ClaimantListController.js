/*
    Created: RAP - February 2017 for March Release
    Purpose: PD-190 - Claimant Summary - List of Claimants
*/

({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLFClaimants");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            component.set("v.claimants", response.getReturnValue()); 
        });
        $A.enqueueAction(action);
        
        var action2 = component.get("c.getOthers");
        action2.setParams({"aId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
        	var state = response.getState();            
        	if (component.isValid() && state === "SUCCESS") {
                component.set("v.others", response.getReturnValue());
	            var clMap = component.get("v.others"); 
	            var result = [];
	            var temp = [];
	            for (var plif in clMap) {
	            	temp = clMap[plif];
	       			result.push({
	       				key: plif,
	       				value: temp
	       			});
	            }
	            component.set("v.kludge", result);
            } else {
                console.log('No referrng firms found');
            }
        });
        $A.enqueueAction(action2);
    }
})