/*  
    Created: Marc Dysart - February 2017 for first release
    Purpose: PD-115 -Kanban Lien - Claimant   

History:

    Updated: lmsw - May 2017
    Purpose: PD-614 - Add method to get Lien record type ID for the "New" button to add a new Lien

*/
({
	hideCards : function(component) {
		 // This shows all the cards first if they were hidden
        var cards = document.getElementsByClassName("hideCard");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.remove("hideCard")
        }

        // Then hides all the cards

        var cards = document.getElementsByClassName("lien-card");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.add("hideCard")
        }
	},

	saveNewStage : function(component, newstage, lienId) {
        var action5 = component.get("c.updateLienCard");
        action5.setParams({
            "lId" : lienId,
            "stage"  : newstage
        });   
        $A.enqueueAction(action5);
    },

     // This shows only the appropriate cards
    showCorrectCards : function(component, cardName) {
    	var cards = document.getElementsByClassName(cardName);
	        for(var i = 0; i < cards.length; ++i){
	            cards.item(i).classList.remove("hideCard");
	        };
    },

    // This changes the button to look selected
    // This can be deleted if it is confirmed that no Filter buttons are needed
    showButtonSelected: function(component, event) {
        var cards = document.getElementsByClassName("slds-button--brand");
            for(var i = 0; i < cards.length; ++i){
                cards.item(i).classList.remove("slds-button--brand")
                cards.item(i).classList.add("slds-not-selected")
            }
        var thisbutton= event.target;
        $A.util.removeClass(thisbutton, 'slds-not-selected');
        $A.util.addClass(thisbutton, 'slds-button--brand'); 
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
// start PD-614
    getTypeId : function(component) {
        var selectedType = component.get("v.selectedType");
        console.log('recordType: '+ selectedType);
        var action = component.get("c.getLienTypeId");
        action.setParams({"lienType": selectedType});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state ==="SUCCESS") {
                component.set("v.newLienType", response.getReturnValue());
                console.log('newLienType: '+ response.getReturnValue());
            } else {
                console.log('Problem getting lien type Id, response state: '+ state);
            }
        });
        $A.enqueueAction(action);
    },
    
    showPopup : function(component, divId, className) {
        var modal = component.find(divId);       
        $A.util.removeClass(modal, className+'hide');
        $A.util.addClass(modal, className+'open')       
    },

    hidePopup : function(component, divId, className) {
        var modal = component.find(divId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    }
// end PD-614

})