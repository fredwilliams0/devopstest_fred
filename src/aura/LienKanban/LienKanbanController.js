/*
    Created: Marc Dysart - February 2017 for first release
    Purpose: PD-115 -Kanban Lien - Claimant     
 
 History:
    
    Updated: Marc Dysart - March 2017 for first release
    Purpose: PRODSUPT-10 - Add substage in Kanban View 

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 - Modify to use Lien Record Types

    Updated: Marc Dysart - April 2017
    Purpose: PRODSUPT-83 - Kanban: Adjustments to cards for PLRP and Non PLRP liens.

    Updated: lmsw - May 2017
    Purpose: PD-615 - Add methods for the "New" button to add a new Lien

*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLiens");
        action.setParams({"cId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.liens", response.getReturnValue());
            } else {
                console.log('Problem getting liens, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

        var action2 = component.get("c.getStages");
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.stages", response.getReturnValue());
            } else {
                console.log('Problem getting stages, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);

        var action3 = component.get("c.getLienMap2");
        action3.setParams({"cId": component.get("v.recordId")});
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienMap", response.getReturnValue());
            } else {
                console.log('Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action3);

        var action4 = component.get("c.getLienTypeMap");
        action4.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienTypes", result);
            } else {
                console.log('Problem getting Lien Maps 2, response state: ' + state);
            }
        });
        $A.enqueueAction(action4);
// start PRODSUPT-10  
        var action5 = component.get("c.getLienSubstageMap");
        action5.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienSubstages", result);
            } else {
                console.log('Problem getting Lien Substage Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action5);

// end PRODSUPT-10 

// start PRODSUPT-83  
        var action6 = component.get("c.getLienNonPLRPtype");
        action6.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var clMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in clMap) {
                    temp = clMap[plif];
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienNonPLRPtype", result);
            } else {
                console.log('Problem getting Lien Non-PLRP type Maps, response state: ' + state);
            }
        });
        $A.enqueueAction(action6);
    }, 

// end PRODSUPT-83

    editLien : function(component, event, helper) {
        var lienID = event.target.id;
        var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
          "recordId": lienID
       });
        editRecordEvent.fire();
    },

// start PD-614
    showModal : function(component, event, helper) {
        helper.showPopup(component, 'recordTypeModal', 'slds-fade-in-');
        helper.showPopup(component, 'backdrop', 'slds-backdrop--');        
        helper.getTypeId(component);
    },

    hideModal : function(component, event, helper) {
        helper.hidePopup(component, "recordTypeModal", 'slds-fade-in-');
        helper.hidePopup(component, 'backdrop', 'slds-backdrop--');
    },
    
    onRadio : function(component, event, helper ){
        var selected = event.getSource().get("v.text");
        component.set("v.selectedType", selected);
        helper.getTypeId(component);
    },
    
    createLien : function (component, event, helper) {
        var newType = component.get("v.newLienType");
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Lien__c",
            "recordTypeId": newType
        });
        createRecordEvent.fire();
    },
// end PD-614

    allowDrop: function(cmp, event, helper){
        event.preventDefault();
    },

    drag: function(cmp, ev, helper){
        ev.dataTransfer.setData("text", ev.target.id);
        var draggedLienID = ev.target.id;
        ev.target.classList.add("draggedCard");
    },
    
    drop: function(cmp, ev, helper){
        ev.preventDefault();
        var el = ev.srcElement;
        if ( ev.target.className == "listcontent" ) {
            var newstage = ev.target.id;
            var cards = document.getElementsByClassName("draggedCard");
            for(var i = 0; i < cards.length; ++i){
                var draggedLienID = cards.item(i).id
                helper.saveNewStage(cmp, newstage, draggedLienID);
// start PRODSUPT-10 
                var onecard = cards.item(i);
                var substage_label = onecard.getElementsByClassName("lien-substage")[0];
                if (substage_label != null) {
                   substage_label.classList.add("hide-substage");
                 }
// end PRODSUPT-10 
                cards.item(i).classList.remove("draggedCard")
            }
            var data = ev.dataTransfer.getData("text");
            el.appendChild(document.getElementById(data));
        }
    },

    showLienType: function(component, event, helper){
         var lienNames = document.getElementsByClassName("lienTypeName");
         for(var i = 0; i < lienNames.length; ++i){
            var selected = lienNames.item(i).get("v.value");
            selected = selected.replace("_",/ /g);
        }

    },

    medicaidFilter: function(component, event, helper){
        helper.hideCards(component);
        // This shows only the appropriate cards
        helper.showCorrectCards(component, "Medicaid");
        // This shows the button as selected
        // helper.showButtonSelected(component, event);
        
        // Change label of the filter for dropdown filter
        helper.updateTriggerLabel(component,event);

    },

     medicareFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Medicare Individual Resolution");
        helper.updateTriggerLabel(component,event);
    },

    medicareModeledFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Medicare Global Model");
        helper.updateTriggerLabel(component,event);
    },

    nonPFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Non-PLRP");
        helper.updateTriggerLabel(component,event);
    },

    plrpFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "PLRP Claim Detail");
        helper.updateTriggerLabel(component,event);
    },

    plrpModeledFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "PLRP Model");
        helper.updateTriggerLabel(component,event);     
    },

    othergovtFilter: function(component, event, helper){
        helper.hideCards(component);
        helper.showCorrectCards(component, "Other Govt"); 
        helper.updateTriggerLabel(component,event);
    },

    allLienFilter: function(component, event, helper){
        helper.hideCards(component);    
        var cards = document.getElementsByClassName("lien-card");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.remove("hideCard")
        }
        helper.updateTriggerLabel(component,event);
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
    updateLabel: function(cmp, event) {
        var triggerCmp = cmp.find("mytrigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    },
    getMenuSelected: function(cmp) {
        var menuItems = cmp.find("checkbox");
        var values = [];
        for (var i = 0; i < menuItems.length; i++) {
            var c = menuItems[i];
            if (c.get("v.selected") === true) {
                values.push(c.get("v.label"));
            }
        }
        var resultCmp = cmp.find("result");
        resultCmp.set("v.value", values.join(","));
    },
    getRadioMenuSelected: function(cmp) {
        var menuItems = cmp.find("radio");
        var values = [];
        for (var i = 0; i < menuItems.length; i++) {
            var c = menuItems[i];
            if (c.get("v.selected") === true) {
                values.push(c.get("v.label"));
            }
        }
        var resultCmp = cmp.find("radioResult");
        resultCmp.set("v.value", values.join(","));
    }


})