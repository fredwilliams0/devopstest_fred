<!--  
	Created: Marc Dysart - February 2017 for first release
	Purpose: PD-115 -Kanban Lien - Claimant    

History:

	Updated: Marc Dysart - March 2017 for first release
	Purpose: PRODSUPT-10 - Add substage in Kanban View  
	
	Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 - Modify to use Lien Record Types

	Updated: Marc Dysart - April 2017
    Purpose: PRODSUPT-83 - Kanban: Adjustments to cards for PLRP and Non PLRP liens.
    
    Updated: lmsw - May 2017
    Purpose: PD-664 Modify filter for No Lien column

    Updated: lmsw - May 2017
    Purpose: PD-614 - Add 'New' button to create a new Lien    

    Updated: Marc Dysart - May 2017
    Purpose: PD-641 - On the Claimant Page, on the Kanban, Add Dates to the Submitted Column
 
-->

<aura:component controller="Controller_Aura1" access="global" implements="flexipage:availableForAllPageTypes,force:hasRecordId">
	<aura:attribute name="liens" type="Lien__c[]" default="{'sobjectType': 'Lien_c[]'}"/>
	<aura:attribute name="lienMap" type="Map" access="global"/>
	<aura:attribute name="stages" type="list" access="global"/>
	<aura:attribute name="lienTypes" type="list" access="global"/> 
<!-- PD-614  -->
    <aura:attribute name="newLienType" type="String" access="global" />
    <aura:attribute name="selectedType" type="String" access="global" default="Medicaid"/>
<!-- start PRODSUPT-10  -->
	<aura:attribute name="lienSubstages" type="list" access="global"/>
<!-- start PRODSUPT-10  -->
<!-- start PRODSUPT-83  -->
	<aura:attribute name="lienNonPLRPtype" type="list" access="global"/>
<!-- start PRODSUPT-83  -->
	<aura:attribute name="component_description" type="String" default="Component Description" access="global"/>
	<aura:handler name="init" value="{!this}" action="{!c.doInit}" />
	
<!-- *********** -->
<!-- Filter Tabs -->
<!-- *********** -->
<div class="slds-tabs--default">
	<div class="slds-grid">
		<div class="slds-col slds-text-align--left">
			<div class="filter-dropdown" >
		    	<ui:menu >	
				    <ui:menuTriggerLink label="All Liens">
	    				<lightning:button aura:id="trigger" variant="neutral" label="All Liens" iconName="utility:down" iconPosition="right" />
					</ui:menuTriggerLink>
				        <ui:menuList class="actionMenu" aura:id="actionMenu" >
				              <ui:actionMenuItem label="All Liens" click="{!c.allLienFilter}"/>
				              <ui:actionMenuItem label="Medicare Individual Resolution" click="{!c.medicareFilter}"/>
				              <ui:actionMenuItem label="Medicare Global Model" click="{!c.medicareModeledFilter}"/>
				              <ui:actionMenuItem label="Medicaid" click="{!c.medicaidFilter}"/>
				              <ui:actionMenuItem label="Non-PLRP" click="{!c.nonPFilter}"/>
				              <ui:actionMenuItem label="Other Gov't" click="{!c.othergovtFilter}"/>
				              <ui:actionMenuItem label="PLRP Claim Detail" click="{!c.plrpFilter}"/>
				              <ui:actionMenuItem label="PLRP Model" click="{!c.plrpModeledFilter}"/>
				        </ui:menuList> 
				</ui:menu>
			</div> 
		</div>
		<div class="slds-col slds-text-align--right">
<!-- start PD-614 --> 
	        <ui:button label="New Lien" press="{!c.showModal}" />
	        <div class="slds-backdrop slds-backdrop--hide" aura:id="backdrop"></div>
		</div>
<!-- end PD-614 -->
	</div>
</div>


<!-- ************** -->
<!-- Kanban Header  -->
<!-- ************** -->
	<div class="slds-grid">
		<div class="slds-tabs--path" role="application">
		    <ul class="slds-tabs--path__nav" role="tablist">
			    <aura:iteration items="{!v.stages}" var="stage">
				    <li class="slds-tabs--path__item slds-is-current" role="presentation">
				        <a class="slds-tabs--path__link" id="tabs-path-422" aria-controls="content-path-1" aria-selected="false" tabindex="-1" role="tab" href="javascript:void(0);" aria-live="assertive">
				          <span class="slds-tabs--path__title"> {! stage} </span>
				        </a>
				    </li>
			    </aura:iteration>
		    </ul>
		</div>
	</div>

<!-- ************** -->
<!-- Tiles of Liens  -->
<!-- ************** -->

  	<div class="slds-grid">
	  	<td class="listcontent" id="To_Be_Submitted" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  		<aura:iteration items="{!v.lienMap.To_Be_Submitted}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					      <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if> 
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p> 
					      <div class="slds-tile__detail slds-text-body--small">
					        <p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>
					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
	  	<td class="listcontent" id="Submitted" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  	<aura:iteration items="{!v.lienMap.Submitted}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					       <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if>
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p>					      
					      <div class="slds-tile__detail slds-text-body--small">
<!-- start PRODSUPT-10  -->
					      	 	<aura:iteration items="{!v.lienSubstages}" indexVar="key" var="r" >
				        			<aura:if isTrue="{!(lien.Substage__c == r.key)}">
				        				<p class="slds-truncate lien-substage">
				        				{!r.value}   				
<!-- Start PD-641  -->					    <aura:if isTrue="{!(r.value == 'First Notice Sent')}">
					    						<p class="slds-truncate"><ui:outputDate value="{! lien.Date_Submitted__c}"/></p>
											</aura:if>
											<aura:if isTrue="{!(r.value == 'Second Notice Sent')}">
					    						<p class="slds-truncate"><ui:outputDate value="{! lien.Date_Second_Sent__c}"/></p>
											</aura:if>
											<aura:if isTrue="{!(r.value == 'Third Notice Sent')}">
					    						<p class="slds-truncate"><ui:outputDate value="{! lien.Date_Third_Sent__c}"/></p>
<!-- End PD-641  -->						</aura:if>
										</p>
				        			</aura:if>
					        	</aura:iteration>						
<!-- end PRODSUPT-10  -->
					         <p class="slds-truncate"><ui:outputCurrency value="{! lien.LienAmounts__r[0].Lien_Amount__c}"/></p>
					        <p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>
					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
	  	<td class="listcontent" id="No_Lien" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  	<aura:iteration items="{!v.lienMap.No_Lien}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					       <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if>
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p>				      
					      <div class="slds-tile__detail slds-text-body--small">
<!-- start PRODSUPT-10  -->
					      	<p class="slds-truncate lien-substage">
					      	 	<aura:iteration items="{!v.lienSubstages}" indexVar="key" var="r" >
				        			<aura:if isTrue="{!(lien.Substage__c == r.key)}">
				        				{!r.value}
				        			</aura:if>
					        	</aura:iteration>
							</p>
<!-- end PRODSUPT-10  -->
					         <p class="slds-truncate"><ui:outputCurrency value="{! lien.LienAmounts__r[0].Lien_Amount__c}"/></p>
					        <p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>
					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
	  	<td class="listcontent" id="Asserted" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  		<aura:iteration items="{!v.lienMap.Asserted}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					      <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if>
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p>					      
					      <div class="slds-tile__detail slds-text-body--small">
<!-- start PRODSUPT-10  -->
					      	<p class="slds-truncate lien-substage">
					      	 	<aura:iteration items="{!v.lienSubstages}" indexVar="key" var="r" >
				        			<aura:if isTrue="{!(lien.Substage__c == r.key)}">
				        				{!r.value}
				        			</aura:if>
					        	</aura:iteration>
							</p>
<!-- end PRODSUPT-10  -->
					        <p class="slds-truncate"><ui:outputCurrency value="{! lien.LienAmounts__r[0].Lien_Amount__c}"/></p>
					        <p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>
					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
	  	<td class="listcontent" id="Audit" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  	<aura:iteration items="{!v.lienMap.Audit}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					      <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if>
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p>					      
					      <div class="slds-tile__detail slds-text-body--small">
<!-- start PRODSUPT-10  -->
					      	<p class="slds-truncate lien-substage">
					      	 	<aura:iteration items="{!v.lienSubstages}" indexVar="key" var="r" >
				        			<aura:if isTrue="{!(lien.Substage__c == r.key)}">
				        				{!r.value}
				        			</aura:if>
					        	</aura:iteration>
							</p>
<!-- end PRODSUPT-10  -->
					        <p class="slds-truncate"><ui:outputCurrency value="{! lien.LienAmounts__r[0].Lien_Amount__c}"/></p>
					        <p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>
					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
	  	 <td class="listcontent" id="Final" ondrop="{!c.drop}" ondragover="{!c.allowDrop}">
	  	<aura:iteration items="{!v.lienMap.Final}" var="lien">
		  		<ul class="{! lien.RecordType.Name + ' slds-has-dividers--around-space lien-card'}" draggable="true" ondragstart="{!c.drag}" id="{! lien.Id}">
				  	<li class="slds-item">
					    <div class="slds-tile slds-tile--board">
					      <h3 class="slds-truncate" title="{! lien.Name}">
					      	<a href="{! '/' + lien.Id }">
					      		<p class="slds-truncate">
					        		<aura:iteration items="{!v.lienTypes}" indexVar="key" var="r" >
<!-- PRODSUPT-57  -->      				<aura:if isTrue="{!(lien.RecordType.DeveloperName == r.key)}">
					        				{!r.value}
					        			</aura:if>
					        		</aura:iteration>
							    </p>
							</a>
						  </h3>
<!-- START PRODSUPT-83 --><aura:if isTrue="{!(lien.RecordType.Name == 'Non-PLRP')}">
							<p class="slds-truncate">
				        		<aura:iteration items="{!v.lienNonPLRPtype}" indexVar="key" var="r" >
				      				<aura:if isTrue="{!(lien.Non_PLRP_Type__c == r.key)}"> 
				        				{!r.value}
				        			</aura:if>
				        		</aura:iteration>
			        		</p>
<!-- END PRODSUPT-83 --> </aura:if>
					      <h3 class="slds-truncate">{! lien.Account__r.Name}</h3>
<!-- PRODSUPT-83  -->	  <p class="slds-truncate">{! lien.Health_Plan__c}</p>					      
					      <div class="slds-tile__detail slds-text-body--small">
<!-- start PRODSUPT-10  -->
					      	<p class="slds-truncate lien-substage">
					      	 	<aura:iteration items="{!v.lienSubstages}" indexVar="key" var="r" >
				        			<aura:if isTrue="{!(lien.Substage__c == r.key)}">
				        				{!r.value}
				        			</aura:if>
					        	</aura:iteration>
							</p>
<!-- end PRODSUPT-10  -->
					        <aura:if isTrue="{!(lien.Override_Amount__c == null)}">
					        	<p class="slds-truncate"><ui:outputCurrency value="{! lien.LienAmounts__r[0].Lien_Amount__c}"/></p>
					        	<p class="slds-truncate"><ui:outputDate value="{! lien.LienAmounts__r[0].Lien_Amount_Date__c}"/></p>
								<aura:set attribute="else">
					                <p class="slds-truncate">Overridden:<ui:outputCurrency value="{! lien.Override_Amount__c}"/></p>
					                <p class="slds-truncate"><ui:outputDate class="lien-card-date" value="{! lien.Date_Overridden__c}"/></p> 
				                </aura:set>
				            </aura:if>
<!-- PRODSUPT-83  -->		<aura:if isTrue="{!(lien.Final_Payable__c != null)}">
								<p class="slds-truncate">Final Payable Amount</p>
								<p class="slds-truncate"><ui:outputCurrency value="{! lien.Final_Payable__c}"/></p>
<!-- PRODSUPT-83  -->		</aura:if>   	
					        <div class="kanban-edit-lien"><button class="slds-button slds-button--neutral slds-not-selected" onclick="{!c.editLien}" id="{! lien.Id}">Edit</button></div>

					      </div>
					    </div>
				  	</li>
		  		</ul>
	  		</aura:iteration>
	  	</td>
  	</div>
<!-- start PD-614 --> 
<!-- ********************** -->
<!-- Lien Record Type Modal -->
<!-- ********************** -->		 
    <div  aria-hidden="true" role="dialog" class="slds-modal slds-fade-in-hide" aura:id="recordTypeModal">
    
	        <div class="modal_container slds-modal__container">
	            <div class="modal header slds-modal__header slds-theme--info">  
	                <lightning:buttonIcon variant="bare" size="large" class="slds-modal__close slds-button--icon-inverse" iconName="utility:close" onclick="{! c.hideModal }" alternativeText="Close window" />
	                <h2 class="slds-text-heading--medium">New Lien</h2>
	            </div>
	            <div class="scrollable slds-modal__content slds-p-around--medium">
	            	<div class="changeRecordTypeRow">
	                    <div class="slds-modal__container" aura:id="lienRecordNew" style="margin-top:2rem;">
	                        <fieldset class="slds-form-element">
	                            <div class="changeRecordTypeLeftColumn">
	                            	<legend class="form-element__legend slds-form-element__legend">Select a record type</legend>
	                            </div>
	                        	<div class="changeRecordTypeRightColumn slds-form-element__control" aura:id="lienRecordType">
	                                <ui:inputRadio  aura:id="radioMedicaid" text="Medicaid" name="radioType" change="{!c.onRadio}" value="true"/>
	                                	<label class="slds-radio__label" for="radio_PrimaryContact">
	                                		<span class="slds-radio--faux"></span>
                        					<span class="slds-form-element__label">Medicaid</span>
                    					</label>
	                                <aura:iteration items="{!v.lienTypes}" var="item" >
	                                	<div class="uiInput uiInputRadio">
	                                		<aura:if isTrue="{!item.value != 'Medicaid'}">
			                                	<ui:inputRadio  text="{!item.value}" name="radioType" change="{!c.onRadio}"/>
			                                	<label class="slds-radio__label" for="radio_PrimaryContact">
			                                		<span class="slds-radio--faux"></span>
		                        					<span class="slds-form-element__label">{!item.value}</span>
		                    					</label>
			                    			</aura:if>
	                					</div>
	                                </aura:iteration>
	                            </div>
	                        </fieldset>                    
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer slds-modal__footer">
	                <lightning:button variant="neutral" label="Cancel"  class="slds-button slds-button--neutral .slds-modal__close" onclick="{! c.hideModal }" />
	                <lightning:button variant="brand" label="Next" onclick="{!c.createLien}" />
	            </div>
	        </div>

	</div>
<!-- end PD-614 -->
</aura:component>