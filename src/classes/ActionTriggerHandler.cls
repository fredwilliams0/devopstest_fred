/*
    Created: MEF - January 2017 for first release
    Purpose: Handler for Action trigger events
    ToDo: move utility methods to separate classes as appropriate 

 History: 

 	Updated: lmsw - March 2017 for March Release
 	Purpose: Add Method to create Box Folder when an Action is created.
*/

public with sharing class ActionTriggerHandler {

    public void onAfterInsert(list<Action__c> newActions, map<Id, Action__c> newMap) {
    	set<Id> actionIds = newMap.keySet();
    	createBoxFolder(actionIds);
	}
         
//	public void onBeforeUpdate(List<Action__c> newActionRecs, Map<Id, Action__c> newActionsMap, Map<Id, Action__c> oldActionsMap) {
//	}

//	public void onBeforeInsert(List<Action__c> newActionRecs, Map<Id, Action__c> newActionsMap) {
//	}

//	public void onAfterUpdate(List<Action__c> newActionRecs, List<Action__c> oldActionRecs, Map<Id, Action__c> newActionsMap, Map<Id, Action__c> oldActionsMap) {
//	}
    
@future (callout=true)
    private static void createBoxFolder(set<Id> aIds) {
        box.Toolkit boxToolkit = new box.Toolkit(); // Instantiate the Toolkit object
        for (Id aId : aIds) // Create a folder and associate it with an Action
        	string actionFolderId = boxToolkit.createFolderForRecordId(aId,null,true);
        if (!test.isRunningTest()) boxToolkit.commitChanges();
    }
}