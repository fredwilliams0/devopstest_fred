/*
	Created: MEF - January 2017 for first release
	Purpose: test coverage for AwardTrigggerHandler class

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change Description__c to Description2__c)
					  Coverage as of 3/27/17 - 91% 
*/

@isTest
private class AwardTriggerHandlerTests {
	
	static testMethod void testAwardNaming() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
    	Action__c action = new Action__c(Name = 'Test Action',
    									 Active__c = true);
		insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
		Account a = new Account(Name = 'Test Account',
    							RecordTypeId = rtId,
    							BillingStreet = '123 Main St',
    							BillingCity = 'Denver',
    							BillingState = 'CO',
    							BillingPostalCode = '80202');
    	insert a;
    	Contact c = new Contact(LastName = 'Attorney',
    							FirstName = 'Test',
    							email = 'tattorney@lawfirm.com');
    	insert c;
		Claimant__c c1 = new Claimant__c(Address_1__c = '123 Elm St',
										 City__c = 'Denver',
										 Email__c = 'tclaimant@injured.com',
										 Law_Firm__c = a.Id,
										 First_Name__c = 'Test',
										 Last_Name__c = 'Claimant',
										 Phone__c = '3035551212',
										 SSN__c = '135461448',
										 State__c = 'CO',
										 Zip__c = '80138');
		insert c1;
		Award__c award1 = new Award__c (
								Name = 'Award1',
								Settlement_Category__c = 'Base',
								Settlement_SubCategory__c = 'Conserve_Claim',
								Action__c = action.Id,
								Amount__c = 250000,
								Claimant__c = c1.Id,
								Date_Of_Award__c = Date.today(),
								Description2__c = 'Test Base Award');
		insert award1;
		
		test.startTest();
		Award__c updatedAward = [SELECT Id, Name FROM Award__c LIMIT 1];
		system.assertEquals('Test Claimant - Test Base Award - Test Action', updatedAward.Name);
		test.stopTest();
	} //end testAwardNaming method
} //end class