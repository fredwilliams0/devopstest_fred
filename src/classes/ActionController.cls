/*  
	Created: RAP - April 2017 for May Release
    Purpose: PD-208 - View Multiple Actions

	Updated: RAP April 2017 for May Release
	Purpose: techDebt - Operations Flow Control Prep
    
*/

public without sharing class ActionController {

	public static Id userId{get;set;}
	public static Id aId{get;set;}
	public static Id existingAID{get;set;}

// constructor
    public ActionController(ApexPages.StandardController stdCtrl) {
		string actionTab;
		string actionName;
		userId = test.isRunningTest() ? [SELECT Id FROM User WHERE LastName = 'User' limit 1].Id : userInfo.getUserId();
		system.debug(loggingLevel.INFO, 'RAP --->>1 userId = ' + userId);
		existingAID = ActionAssignment2__c.getInstance(userId).Action__c;
		system.debug(loggingLevel.INFO, 'RAP --->>1 Existing AID = ' + existingAID);
		actionTab = ApexPages.currentPage().getParameters().get('sfdc.tabName');
		system.debug(loggingLevel.INFO, 'RAP --->> actionTab = ' + actionTab);
    	actionName = AppToAction__c.getInstance(actionTab).Action__c;
		aId = [SELECT ActionID_18__c FROM Action__c WHERE Name = :actionName limit 1].ActionID_18__c;
		system.debug(loggingLevel.INFO, 'RAP --->>1 AID = ' + aId);
    }
// page auto-run method
    public pageReference RedirectToAction() {
		if (existingAID != aId) {
			SwapShares();
			Controller_Aura1.SetAction(aId);
		}
		return new pageReference('/' + aId);
    }
// start PD-208
	public static void SwapShares() {
		ShareRecords();
		DeleteShares();
	}
	
	private static void ShareRecords() {
		list<Account> accList;
		try {
			accList = [SELECT Id, OwnerId FROM Account WHERE Id IN (SELECT Account__c FROM Action_Account__c WHERE Action__c = :aId)];
		}
		catch (exception e) {}
		map<Id,string> checkMap = new map<Id,string>();
	    list<AccountShare> accShareList = new list<AccountShare>();
	    list<Action__Share> asList = [SELECT ParentId, AccessLevel FROM Action__Share WHERE UserOrGroupId = :userId];
	    for (Action__Share asVar : asList)
	    	checkMap.put(asVar.ParentId, asVar.AccessLevel);
	    Action__Share actShare;
	    if (!checkMap.containsKey(aId) || (checkMap.get(aId) != 'Edit' && checkMap.get(aId) != 'All')) {
		    actShare = new Action__Share(ParentId = aId,
										 AccessLevel = 'Edit',
										 UserOrGroupId = userId);
		}
	    for (Account acc : accList) {
	    	if (acc.OwnerId != userId) {
				accShareList.add(new AccountShare(AccountId = acc.Id,
												  AccountAccessLevel = 'Edit',
												  OpportunityAccessLevel = 'Read',
												  UserOrGroupId = userId));
	    	}
		}
		system.debug(loggingLevel.INFO, 'RAP --->> accShareList size: ' + accShareList.size());
		try {
			if (actShare != null)
				insert actShare;
			if (!accShareList.isEmpty())
				insert accShareList;
		} 
		catch (Exception e) {
			system.debug('>>>Error: '+e);
		}
	}
	private static void deleteShares() {
		list<Action__Share> actList = [SELECT Id, UserOrGroupId
									   FROM Action__Share
									   WHERE UserOrGroupId = :userId
									   AND ParentId = :existingAID
									   AND RowCause = 'Manual'];	    
		list<AccountShare> accList = [SELECT Id, UserOrGroupId
									  FROM AccountShare
									  WHERE UserOrGroupId = :userId
									  AND AccountId IN (SELECT Account__c FROM Action_Account__c WHERE Action__c = :existingAID)
									  AND RowCause = 'Manual'];	    
		if (actList != null && !actList.isEmpty())
			delete actList;
		if (accList != null && !accList.isEmpty())
			delete accList;
	}
// end PD-208    
}