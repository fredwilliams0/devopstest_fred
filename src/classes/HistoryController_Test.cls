/*
    Created: RAP - March 2017 for March Release
    Purpose: test functions of History Controllers

History: 

    Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claamant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP - April 2017 for May Release
    Purpose: Fix failing testMethod
	         Coverage as of 4/25 - ActionHistoryController: 100%
	                               ClaimantHistoryController: 100%
	                               LienHistoryController: 100%
*/
@isTest

private class HistoryController_Test {

@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
		Id rtId_lien = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicaid').getRecordTypeId();
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = false);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        action.Law_Firm__c = acc.Id;
        update action;
        action.Active__c = true;
        update action;
        Claimant__c claim = new Claimant__c(Address_1__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
        claim.First_Name__c = 'RAPPER';
        update claim;
        claim.Last_Name__c = 'RAP';
        update claim;
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = action.Id, 
                                   RecordTypeId = rtId_lien,
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Date_Submitted__c = system.today(), 
                                   Notes__c = 'This is a note', 
                                   Stages__c = 'Submitted', 
                                   State__c = 'CO', 
                                   Submitted__c = true);
		insert lien;
		lien.State__c = 'AZ';
		update lien;
		lien.Cleared__c = true;
		update lien;
	}
	
	static testMethod void TestHistories() {
		Lien__c l = [SELECT Id, Claimant__r.Id, Action__r.Id FROM Lien__c WHERE RecordType.Name = 'Medicaid' limit 1];
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(l.Action__r);
		ActionHistoryController ctrl = new ActionHistoryController(stdCtrl);
		list<Action__History> aH = ctrl.actionH;
		
		stdCtrl = new ApexPages.StandardController(l.Claimant__r);
		ClaimantHistoryController ctrl2 = new ClaimantHistoryController(stdCtrl);
		list<Claimant__History> cH = ctrl2.claimantH;

		stdCtrl = new ApexPages.StandardController(l);
		LienHistoryController ctrl3 = new LienHistoryController(stdCtrl);
		list<Lien__History> lH = ctrl3.lienH;
	}
}