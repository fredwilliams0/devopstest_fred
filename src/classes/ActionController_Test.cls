/*
	Created: RAP - April 2017 for May Release
    Purpose: PD-208 - View Multiple Actions

 */
@isTest
private class ActionController_Test {

@testSetup
	static void CreateData() {
		Id pId = [SELECT Id FROM Profile WHERE Name = 'Customer Service' limit 1].Id;
		User u = new User(UserName = 'TestUser@structures.com',
						  isActive = true,
						  ProfileId = pId,
						  LastName = 'User', 
						  Email = 'rap@rap.com', 
						  Alias = 'tuser', 
						  TimeZoneSidKey = 'America/Denver', 
						  LocaleSidKey = 'En_US', 
						  EmailEncodingKey = 'ISO-8859-1', 
						  LanguageLocaleKey = 'en_US');
		insert u;
		system.debug(loggingLevel.INFO, 'RAP --->>1 userId = ' + u.Id);
    	Id rtId = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Non-PLRP').getRecordTypeId();
    	Id rtAc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
    	appToAction__c ata = new appToAction__c(Name = 'abcdefghij',
		    									Action__c = 'Test Action 1');
    	insert ata;
        Action__c actionGood = new Action__c(Name = 'Test Action 1',
	                                         Active__c = true);
        Action__c actionBad = new Action__c(Name = 'Test Action 2',
	                                        Active__c = true);
        insert new list<Action__c>{actionGood,actionBad};
	    Action__Share actShare = new Action__Share(ParentId = actionBad.Id,
												   AccessLevel = 'Edit',
												   UserOrGroupId = u.Id);
		insert actShare;
		ActionAssignment2__c aac1 = new ActionAssignment2__c(Name = u.Id,
															 Action__c = actionBad.Id);
		ActionAssignment2__c aac2 = new ActionAssignment2__c(Name = userInfo.getUserId(),
															 Action__c = actionGood.Id);
		insert new list<ActionAssignment2__c>{aac1,aac2};
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtAc,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
		Action_Account__c aa = new Action_Account__c(Action__c = actionGood.Id,
													 Account__c = acc.Id);
		insert aa;
	    AccountShare accShare = new AccountShare(AccountId = acc.Id,
												 AccountAccessLevel = 'Edit',
												 OpportunityAccessLevel = 'Read',
												 UserOrGroupId = u.Id);
		insert accShare;
		Fee_Model__c model = new Fee_Model__c(Action__c = actionGood.Id,
											  MGM_Additional__c = 250, 
											  M_Additional__c = 250, 
											  Award_Type__c = 'Base',
											  PLRP_Lien_Award__c = 350, 
											  Medicaid_First__c = 350, 
											  NonP_First_Lien__c = 350, 
											  OG_First_Lien__c = 350, 
											  MIR_Flat_Fee__c = 350, 
											  NonP_Flat_Fee__c = 350, 
											  OG_Flat_Fee__c = 350, 
											  MGM_Flat_Fee__c = 350, 
											  PLRP_Flat_Fee__c = 350, 
											  VOE_Med_Med__c = 350, 
											  M_M_Combo__c = 350, 
											  VOE_PLRP__c = 350, 
											  NonP_No_Interest__c = 250,
											  OG_No_Interest__c = 250, 
											  OG_Second_Lien__c = 350, 
											  NonP_Second_Lien__c = 350, 
											  MIR_Split__c = 300);
		insert model;
		Time_Processing__c tpc = new Time_Processing__c(Name = actionGood.Id,
														Second_Notice_Interval__c = 30,
														Third_Notice_Interval__c = 30);
		insert tpc; 
        Claimant__c claim = new Claimant__c(Address_1__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
// start PD-566
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = actionGood.Id,
                                   RecordTypeId = rtId, 
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Notes__c = 'This is a note', 
                                   Stages__c = 'To_Be_Submitted', 
                                   State__c = 'CO', 
                                   Submitted__c = false);
        Lien__c lien1 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = actionGood.Id,
                                    RecordTypeId = rtId, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien2 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = actionGood.Id,
                                    RecordTypeId = rtId, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    State__c = 'CO', 
                                    Submitted__c = true);
		insert new list<Lien__c>{lien,lien1,lien2};
		Award__c a1 = new Award__c(Claimant__c = claim.Id,
								   Action__c = actionGood.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #1',
								   Amount__c = 190000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a2 = new Award__c(Claimant__c = claim.Id,
								   Action__c = actionGood.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #2',
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		insert new list<Award__c>{a1,a2};
		AwardLien__c al1 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien.Id, 
											Award__c = a1.Id);
		AwardLien__c al2 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien1.Id, 
											Award__c = a2.Id);
		AwardLien__c al3 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien2.Id, 
											Award__c = a2.Id);
		insert new list<AwardLien__c>{al1,al2,al3};	
	}
	
    static testMethod void TestStandardRedirect() {
		User u = [SELECT Id, Name FROM User WHERE LastName = 'User' limit 1];
		PageReference pageRef = new PageReference('/apex/ActionRedirect');
		ApexPages.currentPage().getParameters().put('sfdc.tabName','abcdefghij');
		Action__c a = [SELECT Id FROM Action__c WHERE Name = 'Test Action 1' limit 1];
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(a);
		test.startTest();
		system.runAs(u) {
			ActionController ctrl = new ActionController(stdCtrl);
	        ctrl.RedirectToAction();
		}
        test.stopTest();
    }
}