/*
    Created - RAP May 2017 for May22 Patch
    Purpose - Creation of Lien__Shares when changing Actions
    
*/
global class LienShareBatch implements Database.Batchable<SObject>, Database.Stateful {


	public Id aId{get;set;}
	public Id uId{get;set;}
	public boolean add{get;set;}
	
    global LienShareBatch(Id act, Id userId, boolean share) {
    	aId = act;
    	uId = userId;
    	add = share;
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		if (add) {
			list<Lien__c> lList;
			set<Id> shareSet = new set<Id>();
		    list<Lien__Share> lCheckList = [SELECT AccessLevel, ParentId 
		    								FROM Lien__Share 
		    								WHERE UserOrGroupId = :uId
		    								AND ParentId IN (SELECT Id FROM Lien__c WHERE Action__c = :aId)
		    								AND RowCause = 'Manual'];
			for (Lien__Share ls : lCheckList)
				shareSet.add(ls.ParentId);
			return Database.getQueryLocator('SELECT Id FROM Lien__c WHERE Action__c = :aId AND Id NOT IN :shareSet');
		}
		else
			return Database.getQueryLocator('SELECT Id FROM Lien__c WHERE Action__c = :aId');    
	}

	public void execute(Database.BatchableContext BC, list<Lien__c> liens) {
		if (add) {
		    list<Lien__Share> lShareList = new list<Lien__Share>();
		    for (Lien__c l : liens) {
				lShareList.add(new Lien__Share(ParentId = l.Id,
											   AccessLevel = 'Edit',
											   UserOrGroupId = uId));
			}
			if (!lShareList.isEmpty())
				insert lShareList;
			system.debug(loggingLevel.INFO, 'RAP --->> ' + lShareList.size() + ' shares added');
		}
		else {
			list<Lien__Share> lsList = [SELECT Id, UserOrGroupId
										FROM Lien__Share
										WHERE UserOrGroupId = :uId
										AND ParentId IN (SELECT Id FROM Lien__c WHERE Action__c = :aId)
										AND RowCause = 'Manual'];
			if (lsList != null && !lsList.isEmpty())
				delete lsList;
			system.debug(loggingLevel.INFO, 'RAP --->> ' + lsList.size() + ' shares deleted');
		}
	}

	public void finish(Database.BatchableContext BC) {}
}