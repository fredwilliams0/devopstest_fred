/*
    Created - RAP February 2017 for March Release
    Purpose - PD-434 - Tracking time between Lien substages (Batch processor grabs all Liens in the action and updates them, causing
    				   the trigger to calculate substage times and status.)
History:

	Updated: lmsw - March 2017 for May Release
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

	Updated: RAP - April 2017 for May Release
    Purpose: techDebt - change to allow entry of Action Name instead of ID
*/
global class LienBatch implements Database.Batchable<Lien__c> {

	public string action{get;set;}
	
    public LienBatch(string act) {
    	action = act;
    }
    
	global Iterable<Lien__c> start(Database.BatchableContext BC) {
		return [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c
				FROM Lien__c
				WHERE Action__r.Name = :action]; // RAP techDebt
	}

	public void execute(Database.BatchableContext BC, list<Lien__c> liens) {
		if (liens != null && !liens.isEmpty())
			update liens;
	}
	
	public void finish(Database.BatchableContext BC) {
		
	}
}