/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of List Controllers

History: 

	Updated: lmsw - March 2017 for March Release
    Purpose: Change Claimant__c.Address__c to Claimant__c.Address_1__c

    Updated: lmsw - March 2017 for May Release
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP - March 2017 for May Release
    Purpose: PD-520 - Extraneous Field Clean-up (change to Description2__c) + minor refactor

    Updated: RAP - April 2017 for May Release
    Purpose: Fix failing testMethod

    Updated: RAP - May 2017 for May15 Release
    Purpose: PD-571 - Quality Control Implementation
    		 Coverage as of 5/5 - LienBatch: 100%
    		 					  DistributionBatch: 100%
*/
@isTest
private class Batch_Test {
    
@testSetup
	static void CreateData() {
		Date today = system.today();
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Id rtId_lien = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Other Govt').getRecordTypeId();
        Id rtId_lien2 = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicaid').getRecordTypeId();
        Id rtId_lien3 = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicare Global Model').getRecordTypeId();
        Id rtId_lien4 = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('PLRP Model').getRecordTypeId();
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = false);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        DisbursementDates__c ddc = new DisbursementDates__c(Name = 'Test Action',
        													Date__c = today.addDays(3));
        insert ddc;
        Claimant__c c = new Claimant__c(Address_1__c = '123 Main St',
										City__c = 'Denver',
										Email__c = 'rap@rap.com',
										Law_Firm__c = acc.Id,
										First_Name__c = 'RAP',
										Last_Name__c = 'RAPPER',
										Phone__c = '3035551212',
										SSN__c = '135461448',
										Date_SSN_Changed__c = today.addDays(-3),
										State__c = 'CO',
										Opted_in_PLRP__c = 'Yes',
										DOB__c = date.parse('01/02/1940'),
										Status__c = 'Not_Ready_to_be_Cleared',
										Zip__c = '80138');
        insert c;
		Health_Plan__c hp = new Health_Plan__c(Claimant__c = c.Id,
											   PLC_Determination__c = null);
		insert hp;
		Award__c a1 = new Award__c(Claimant__c = c.Id,
								   Action__c = action.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = today, 
								   Description2__c = 'Award #1', 
								   Amount__c = 29000, 
								   Distribution_Status__c = 'Released_to_a_Holdback');
		Award__c a2 = new Award__c(Claimant__c = c.Id,
								   Action__c = action.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = today, 
								   Description2__c = 'Award #2', 
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Cleared');
		Award__c a3 = new Award__c(Claimant__c = c.Id,
								   Action__c = action.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = today, 
								   Description2__c = 'Award #3', 
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Cleared');
		insert new list<Award__c>{a1,a2,a3};
		Fee_Model__c fm = new Fee_Model__c(Action__c = action.Id,
										   Award_Type__c = 'Base',
										   VOE_Med_Med__c = 450.00,
										   OG_Flat_Fee__c = 500.00,
										   Medicaid_First__c = 350.00);
		insert fm;
		Model_Detail__c md1 = new Model_Detail__c(Action__c = action.Id,
												  Provider__c = acc.Id,
												  PayerType__c = 'Primary_Group_Health_Plan',
												  Award_Type__c = 'Base',
												  Covered_Awards__c = 'All',
												  Eligibility__c = 'Entitled_Prior_No_Other_Primary',
												  Cap_Holdback__c = 0.2,
												  Payable_Amount__c = 10000.00,
											 	  Reduction__c = 0.4);
		Model_Detail__c md2 = new Model_Detail__c(Action__c = action.Id,
												  Provider__c = acc.Id,
												  PayerType__c = 'Primary_Part_C',
												  Award_Type__c = 'Base',
												  Covered_Awards__c = 'All',
												  Eligibility__c = 'Entitled_Prior_Other_Primary_Prior',
												  Cap_Holdback__c = 0.2,
												  Payable_Amount__c = 10000.00,
												  Reduction__c = 0.4);
		insert new list<Model_Detail__c>{md1,md2};
        Lien__c l1 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id,
                                 RecordTypeId = rtId_lien, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Notes__c = 'This is a note', 
                                 Stages__c = 'Final', 
                                 Date_Submitted__c = today.addDays(-4),
                                 State__c = 'CO', 
                                 Final_Payable__c = 8765.99,
                                 Submitted__c = true);
        Lien__c l2 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id,
                                 RecordTypeId = rtId_lien2, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Notes__c = 'This is also a note', 
                                 Stages__c = 'Final', 
                                 State__c = 'CO', 
                                 Date_Submitted__c = today,
                                 Final_Payable__c = 9874.11,
                                 Submitted__c = true);
        Lien__c l3 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id,
                                 RecordTypeId = rtId_lien3, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Notes__c = 'This is also a note', 
                                 Stages__c = 'No_Lien', 
                                 Substage__c = 'Not_Eligible',
                                 State__c = 'CO', 
                                 Final_Payable__c = 5887.22,
                                 PayerType__c = 'Primary_Group_Health_Plan',
                                 Submitted__c = true);
        Lien__c l4 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id,
                                 RecordTypeId = rtId_lien4, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Notes__c = 'This is also a note', 
                                 Stages__c = 'Final', 
                                 State__c = 'CT', 
                                 Final_Payable__c = 5887.22,
                                 PayerType__c = 'Primary_Group_Health_Plan',
                                 Submitted__c = true);
        Lien__c l5 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id,
                                 RecordTypeId = rtId_lien4, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Notes__c = 'This is also a note', 
                                 Stages__c = 'No_Lien', 
                                 State__c = 'CT', 
                                 Final_Payable__c = 6337.22,
                                 PayerType__c = 'Primary_Group_Health_Plan',
                                 Submitted__c = true);
		insert new list<Lien__c>{l1,l2,l3,l4,l5};
		LienEligibilityDate__c led1 = new LienEligibilityDate__c(Lien__c = l1.Id,
																 Date_Start__c = today.addDays(-45),
																 Date_End__c = today,
																 LienType__c = 'PartC');
		insert led1;
/*		Lien_Negotiated_Amounts__c lna1 = new Lien_Negotiated_Amounts__c(Lien__c = l1.Id,
																		 Lien_Amount__c = 23425.77,
																		 Lien_Amount_Date__c = today,
																		 Phase__c = 'Final');
		Lien_Negotiated_Amounts__c lna2 = new Lien_Negotiated_Amounts__c(Lien__c = l2.Id,
																		 Lien_Amount__c = 21312.56,
																		 Lien_Amount_Date__c = today,
																		 Phase__c = 'Final');
		insert new list<Lien_Negotiated_Amounts__c>{lna1,lna2};*/
		Injury__c i1 = new Injury__c(Name = 'Injury1',
									 Action__c = action.Id, 
                                 	 Claimant__c = c.Id,
                                 	 Injury_Description__c = 'Injury',
									 Compensable__c = 'True', 
									 DOL__c = today.addDays(-25));
		Injury__c i2 = new Injury__c(Name = 'Injury2',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
                                 	 Injury_Description__c = 'Injury',
									 Compensable__c = 'True', 
									 DOL__c = today.addDays(-45));
		Injury__c i3 = new Injury__c(Name = 'Injury3',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
                                 	 Injury_Description__c = 'Injury',
									 Compensable__c = 'True', 
									 DOL__c = today.addDays(-45));
		Injury__c i4 = new Injury__c(Name = 'Injury4',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
                                 	 Injury_Description__c = 'Injury',
									 Compensable__c = 'True', 
									 DOL__c = today.addDays(-45));
		insert new list<Injury__c>{i1,i2,i3,i4};
		InjuryLien__c ijl1 = new InjuryLien__c(Injury__c = i1.Id,
											   Lien__c = l1.Id);
		InjuryLien__c ijl2 = new InjuryLien__c(Injury__c = i2.Id,
											   Lien__c = l2.Id);
		InjuryLien__c ijl3 = new InjuryLien__c(Injury__c = i3.Id,
											   Lien__c = l3.Id);
		InjuryLien__c ijl4 = new InjuryLien__c(Injury__c = i4.Id,
											   Lien__c = l2.Id);
		insert new list<InjuryLien__c>{ijl1,ijl2,ijl3,ijl4};
		InjuryAward__c ia1 = new InjuryAward__c(Injury__c = i1.Id,
												Award__c = a1.Id);
		InjuryAward__c ia2 = new InjuryAward__c(Injury__c = i2.Id,
												Award__c = a1.Id);
		InjuryAward__c ia3 = new InjuryAward__c(Injury__c = i3.Id,
												Award__c = a2.Id);
		InjuryAward__c ia4 = new InjuryAward__c(Injury__c = i4.Id,
												Award__c = a2.Id);
		insert new list<InjuryAward__c>{ia1,ia2,ia3,ia4};
		Award_Cost_Deduction__c acd1 = new Award_Cost_Deduction__c(Award__c = a1.Id,
																   Amount__c = 255,
																   Cost_Deduction_Type__c = 'Court_Fee');
		Award_Cost_Deduction__c acd2 = new Award_Cost_Deduction__c(Award__c = a1.Id,
																   Amount__c = 2500,
																   Cost_Deduction_Type__c = 'Award_Deduction');
		Award_Cost_Deduction__c acd3 = new Award_Cost_Deduction__c(Award__c = a2.Id,
																   Amount__c = 255,
																   Cost_Deduction_Type__c = 'Court_Fee');
		Award_Cost_Deduction__c acd4 = new Award_Cost_Deduction__c(Award__c = a2.Id,
																   Amount__c = 2500,
																   Cost_Deduction_Type__c = 'Award_Deduction');
		insert new list<Award_Cost_Deduction__c>{acd1,acd2,acd3,acd4};
		AwardLien__c al1 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = l1.Id, 
											Award__c = a1.Id);
		AwardLien__c al2 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = l1.Id, 
											Award__c = a2.Id);
		AwardLien__c al3 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = l4.Id, 
											Award__c = a1.Id);
		AwardLien__c al4 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = l5.Id, 
											Award__c = a1.Id);
		AwardLien__c al5 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = l3.Id, 
											Award__c = a1.Id);
		AwardLien__c al6 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = l2.Id, 
											Award__c = a3.Id);
		insert new list<AwardLien__c>{al1,al2,al3,al4,al5,al6};		
	}
	
	static testMethod void TestDistributionBatchFail() {
		test.startTest();
		database.executeBatch(new DistributionBatch('Test Action'));
        DisbursementDates__c ddc = DisbursementDates__c.getInstance('Test Action');
        ddc.Date__c = system.today();
        update ddc;
		database.executeBatch(new DistributionBatch('Test Action'));
		test.stopTest();
	}

	static testMethod void TestDistributionBatchPass() {
		test.startTest();
        DisbursementDates__c ddc = DisbursementDates__c.getInstance('Test Action');
        ddc.Date__c = system.today();
        update ddc;
		Health_Plan__c hp = [SELECT Id, PLC_Determination__c FROM Health_Plan__c limit 1];
		hp.PLC_Determination__c = 'In_PLRP';		
        update hp;
		database.executeBatch(new DistributionBatch('Test Action'));
		test.stopTest();
	}

	static testMethod void TestLienBatch() {
		test.startTest();
		database.executeBatch(new LienBatch('Test Action'));
		test.stopTest();
	}

	static testMethod void TestAwardLienBatch() {
		test.startTest();
		database.executeBatch(new AwardLienBatch('Test Action'));
		test.stopTest();
	}
}