/*
    Created: RAP - March 2017 for May Release
    Purpose: PD-465 - Lien Detail Page - Notes, Tasks, and Phone calls

	Updated: RAP - April 2017 for May Release
	Purpose: Add Fee Model to CreateData method
    		 Code Coverage as of 4/25: CDNoteTrigger 100%
			    		 			   NoteUtility 100%
    
*/
@isTest
private class NoteUtility_Test {

@testSetup
	static void CreateData() {
        Id rtId = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicaid').getRecordTypeId();
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = true);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
		Fee_Model__c model = new Fee_Model__c(Action__c = action.Id,
											  MGM_Additional__c = 250, 
											  M_Additional__c = 250, 
											  Award_Type__c = 'Base',
											  PLRP_Lien_Award__c = 350, 
											  Medicaid_First__c = 350, 
											  NonP_First_Lien__c = 350, 
											  OG_First_Lien__c = 350, 
											  MIR_Flat_Fee__c = 350, 
											  NonP_Flat_Fee__c = 350, 
											  OG_Flat_Fee__c = 350, 
											  MGM_Flat_Fee__c = 350, 
											  PLRP_Flat_Fee__c = 350, 
											  VOE_Med_Med__c = 350, 
											  M_M_Combo__c = 350, 
											  VOE_PLRP__c = 350, 
											  NonP_No_Interest__c = 250,
											  OG_No_Interest__c = 250, 
											  OG_Second_Lien__c = 350, 
											  NonP_Second_Lien__c = 350, 
											  MIR_Split__c = 300);
		insert model;
        Claimant__c claim = new Claimant__c(Address_1__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = action.Id,
                                   RecordTypeId = rtId, 
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Stages__c = 'To_Be_Submitted', 
                                   State__c = 'CO', 
                                   Submitted__c = false);
        insert lien;
	}
	
    static testMethod void TestNoteLinking() {
		Lien__c l = [SELECT Id, Claimant__c FROM Lien__c WHERE Stages__c = 'To_Be_Submitted' limit 1];
		ApexPages.currentPage().getParameters().put('id', l.Id);
    	test.startTest();
    	ContentNote cn = new ContentNote(Title = 'Test Note',
    									 Content = BLOB.valueOf('This is a test note'));
    	insert cn;
    	ContentDocumentLink cdl = new ContentDocumentLink(Visibility = 'AllUsers',
														  ShareType = 'V',
														  LinkedEntityId = l.Id,
														  ContentDocumentId = cn.Id);
		insert cdl;
    	ContentDocument cd = [SELECT Title, Description FROM ContentDocument WHERE Id = :cn.Id];
    	cd.Description = 'Something';
    	update cd;
    	test.stopTest();
    	list<ContentDocumentLink> links = [SELECT Id FROM ContentDocumentLink WHERE ContentDocumentId = :cd.Id AND LinkedEntityId = :l.Claimant__c];
    	system.assertEquals(1, links.size());
    }
}