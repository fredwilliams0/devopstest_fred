/*
    Created: lmsw - March 2017 
    Purpose: Handler for Lien trigger events
    		 ToDo: move utility methods to separate classes as appropriate 
History:

	Updated: RAP May 2017 for May15 Patch
	Purpose: techDebt - comment out unused methods
*/

public with sharing class LienTriggerHandler {

//	public void onAfterInsert(List<Lien__c> newLiens, Map<Id, Lien__c> newLiensMap) {
//	}
         
    public void onBeforeUpdate(List<Lien__c> newLiens, Map<Id, Lien__c> newLiensMap, Map<Id, Lien__c> oldLiensMap) {
        updateHoldBack(newLiens,oldLiensMap);
        }

    public void onBeforeInsert(List<Lien__c> newLiens, Map<Id, Lien__c> newLiensMap) {
    	insertHoldback(newLiens);
        }

//	public void onAfterUpdate(List<Lien__c> newLiens, List<Lien__c> oldLienRecs, Map<Id, Lien__c> newLiensMap, Map<Id, Lien__c> oldLiensMap) {
//	}
    
    private static void updateHoldBack(List<Lien__c> newLiens, Map<Id, Lien__c> oldLiensMap) {
        for(Lien__c l : newLiens){
            Lien__c oldLien = oldLiensMap.get(l.Id);
            if((l.HBPercent__c != oldLien.HBPercent__c) || 
            	(l.HBPercent__c!=null && (l.Final_Lien_Amount_Value__c != oldLien.Final_Lien_Amount_Value__c))) {
                l.HBAmount2__c = (l.Final_Lien_Amount_Value__c>0)?l.Final_Lien_Amount_Value__c*(l.HBPercent__c/100):0;
            } else if(l.HBAmount2__c != oldLien.HBAmount2__c){
                l.HBPercent__c = null;
            }
        }
    }

    private static void insertHoldback(List<Lien__c> newLiens) {
    	for(Lien__c l : newLiens){
            if(l.HBPercent__c != null) {
                l.HBAmount2__c = (l.Final_Lien_Amount_Value__c>0)?l.Final_Lien_Amount_Value__c*(l.HBPercent__c/100):0;
            } else if(l.HBAmount2__c != null){
                l.HBPercent__c = null;              
            }
        }
    }
}