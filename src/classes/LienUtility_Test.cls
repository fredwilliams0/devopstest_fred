/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of Lien Trigger Utility Class - PD-434 and techDebt

History: 

    Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claamant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c

    Updated: RAP April 2017 for May Release
    Purpose: Repair failing testMethod

    Updated: RAP May 2017 for May15 Release
    Purpose: PD-527 - Automation adjustments
			 Coverage as of 3/21 - LienUtility: 94%
			 					   LienTrigger: 100%
			 Coverage as of 5/21 - LienUtility: 94%
			 					   LienTrigger: 100%
*/
@isTest
private class LienUtility_Test {

@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
// start PD-566
		map<string,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
        Id rtId1 = rtMap.get('Medicaid').getRecordTypeId();
        Id rtId2 = rtMap.get('Medicare Global Model').getRecordTypeId();
        Id rtId3 = rtMap.get('Medicare Individual Resolution').getRecordTypeId();
        Id rtId4 = rtMap.get('Non-PLRP').getRecordTypeId();
        Id rtId5 = rtMap.get('PLRP Claim Detail').getRecordTypeId();
        Id rtId6 = rtMap.get('PLRP Model').getRecordTypeId();
        Id rtId7 = rtMap.get('Other Govt').getRecordTypeId();
// end PD-566
		Group grp1 = new Group(Name = 'To Be Submitted',
							   Type = 'Queue');
		Group grp2 = new Group(Name = '30+ Days without Response',
							   Type = 'Queue');
		Group grp3 = new Group(Name = 'First Notice Sent',
							   Type = 'Queue');
		Group grp4 = new Group(Name = 'Second Notice Sent',
							   Type = 'Queue');
		Group grp5 = new Group(Name = 'Third Notice Sent',
							   Type = 'Queue');
		Group grp6 = new Group(Name = 'Asserted',
							   Type = 'Queue');
		Group grp7 = new Group(Name = 'Audit',
							   Type = 'Queue');
		Group grp8 = new Group(Name = 'Final',
							   Type = 'Queue');
		insert new list<Group>{grp1,grp2,grp3,grp4,grp5,grp6,grp7,grp8};
        Action__c action1 = new Action__c(Name = 'Test Action 1',
                                          Holdback_Percentage__c = 0.4,
                                          Active__c = true);
        Action__c action2 = new Action__c(Name = 'Test Action 2',
                                          Active__c = true);
        insert new list<Action__c>{action1,action2};
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action1.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
		Fee_Model__c model = new Fee_Model__c(Action__c = action1.Id,
											  MGM_Additional__c = 250, 
											  M_Additional__c = 250, 
											  Award_Type__c = 'Base',
											  PLRP_Lien_Award__c = 350, 
											  Medicaid_First__c = 350, 
											  NonP_First_Lien__c = 350, 
											  OG_First_Lien__c = 350, 
											  MIR_Flat_Fee__c = 350, 
											  NonP_Flat_Fee__c = 350, 
											  OG_Flat_Fee__c = 350, 
											  MGM_Flat_Fee__c = 350, 
											  PLRP_Flat_Fee__c = 350, 
											  VOE_Med_Med__c = 350, 
											  M_M_Combo__c = 350, 
											  VOE_PLRP__c = 350, 
											  NonP_No_Interest__c = 250,
											  OG_No_Interest__c = 250, 
											  OG_Second_Lien__c = 350, 
											  NonP_Second_Lien__c = 350, 
											  MIR_Split__c = 300);
		insert model;
		Model_Detail__c md1 = new Model_Detail__c(Action__c = action1.Id, 
												  Medicaid_Aggregate__c = false, 
												  Medicare_Aggregate__c = true, 
												  Cap_Holdback__c = 0.2, 
												  Award_Type__c = 'Base',
												  RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Model_Detail__c' AND Name = 'Medicaid'].Id, 
												  Covered_Awards__c = 'All', 
												  Provider__c = acc.Id, 
												  Eligibility__c = 'Entitled_Prior_No_Other_Primary',
												  Payable_Amount__c = 9987.99, 
												  PayerType__c = 'Primary_Group_Health_Plan',
												  Reduction__c = 0.4, 
												  State__c = 'CO');
		Model_Detail__c md2 = new Model_Detail__c(Action__c = action1.Id, 
												  Medicaid_Aggregate__c = true, 
												  Medicare_Aggregate__c = true, 
												  Cap_Holdback__c = 0.2, 
												  Award_Type__c = 'Base',
												  RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Model_Detail__c' AND Name = 'PLRP Model'].Id, 
												  Covered_Awards__c = 'All', 
												  Provider__c = acc.Id, 
												  Eligibility__c = 'Eligible_after_DOL_but_within_timeframe',
												  Payable_Amount__c = 7768.55, 
												  PayerType__c = 'Primary_Group_Health_Plan',
												  Reduction__c = 0.4, 
												  State__c = 'CO');
		Model_Detail__c md3 = new Model_Detail__c(Action__c = action1.Id, 
												  Medicaid_Aggregate__c = true, 
												  Medicare_Aggregate__c = false, 
												  Cap_Holdback__c = 0.2, 
												  Award_Type__c = 'Base',
												  RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Model_Detail__c' AND Name = 'Medicare Global Model'].Id, 
												  Covered_Awards__c = 'All', 
												  Provider__c = acc.Id, 
												  Eligibility__c = 'Entitled_Prior_No_Other_Primary',
												  Payable_Amount__c = 4537.95, 
												  PayerType__c = 'Primary_Group_Health_Plan',
												  Reduction__c = 0.4, 
												  State__c = 'CO');
		Model_Detail__c md4 = new Model_Detail__c(Action__c = action1.Id, 
												  Medicaid_Aggregate__c = true, 
												  Medicare_Aggregate__c = true, 
												  Cap_Holdback__c = 0.2, 
												  Award_Type__c = 'Base',
												  RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Model_Detail__c' AND Name = 'PLRP Claim Detail'].Id, 
												  Covered_Awards__c = 'All', 
												  Provider__c = acc.Id, 
												  Eligibility__c = 'Entitled_Prior_No_Other_Primary',
												  Payable_Amount__c = 7768.55, 
												  PayerType__c = 'Primary_Group_Health_Plan',
												  Reduction__c = 0.4, 
												  State__c = 'CO');
		insert new list<Model_Detail__c>{md1,md2,md3,md4};
		Time_Processing__c tpc = new Time_Processing__c(Name = action1.Id,
														Second_Notice_Interval__c = 30,
														Third_Notice_Interval__c = 30);
		insert tpc; 
        Claimant__c claim = new Claimant__c(Address_1__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
											Opted_in_PLRP__c = 'Yes',
											DOB__c = date.parse('01/02/1940'),
                                            SSN__c = '135461448',
                                            Flat_Fee_Paid__c = false,
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
// start PD-566
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = action2.Id,
                                   RecordTypeId = rtId1, 
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Notes__c = 'This is a note', 
                                   Stages__c = 'To_Be_Submitted', 
                                   PayerType__c = 'Primary_Group_Health_Plan',
                                   State__c = 'CO', 
                                   Submitted__c = false);
        Lien__c lien1 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId1, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien2 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId2, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien3 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId3, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien4 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId4, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien5 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId5, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien6 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId6, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien7 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action1.Id,
                                    RecordTypeId = rtId7, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    Eligible__c = 'Eligible_after_DOL_but_within_timeframe',
                                    State__c = 'CO', 
                                    Submitted__c = true);
		insert new list<Lien__c>{lien,lien1,lien2,lien3,lien4,lien5,lien6,lien7};
		LienEligibilityDate__c led1 = new LienEligibilityDate__c(Lien__c = lien2.Id,
																 LienType__c = 'PartA',
																 Date_Start__c = date.parse('3/3/2012'));
		insert led1;
		Injury__c inj1 = new Injury__c(Claimant__c = claim.Id,
									   Action__c = action1.Id,
									   DOL__c = date.parse('6/3/2012'),
									   Compensable__c = 'True');
		insert inj1;
		InjuryLien__c il1 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien1.Id);
		InjuryLien__c il2 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien2.Id);
		InjuryLien__c il3 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien3.Id);
		InjuryLien__c il4 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien4.Id);
		InjuryLien__c il5 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien5.Id);
		InjuryLien__c il6 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien6.Id);
		InjuryLien__c il7 = new InjuryLien__c(Injury__c = inj1.Id,
											  Lien__c = lien7.Id);
		insert new list<InjuryLien__c>{il1,il2,il3,il4,il5,il6,il7};
		Award__c a1 = new Award__c(Claimant__c = claim.Id,
								   Action__c = action1.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #1',
								   Amount__c = 190000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a2 = new Award__c(Claimant__c = claim.Id,
								   Action__c = action1.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #2',
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a3 = new Award__c(Claimant__c = claim.Id,
								   Action__c = action1.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #3',
								   Amount__c = 200000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a4 = new Award__c(Claimant__c = claim.Id,
								   Action__c = action1.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #4',
								   Amount__c = 150000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a5 = new Award__c(Claimant__c = claim.Id,
								   Action__c = action1.Id,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #4',
								   Amount__c = 50000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		insert new list<Award__c>{a1,a2,a3,a4,a5};
		InjuryAward__c ia1 = new InjuryAward__c(Injury__c = inj1.Id,
												Award__c = a1.Id);
		InjuryAward__c ia2 = new InjuryAward__c(Injury__c = inj1.Id,
												Award__c = a2.Id);
		InjuryAward__c ia3 = new InjuryAward__c(Injury__c = inj1.Id,
												Award__c = a3.Id);
		InjuryAward__c ia4 = new InjuryAward__c(Injury__c = inj1.Id,
												Award__c = a4.Id);
		InjuryAward__c ia5 = new InjuryAward__c(Injury__c = inj1.Id,
												Award__c = a5.Id);
		insert new list<InjuryAward__c>{ia1,ia2,ia3,ia4,ia5};
		AwardLien__c al1 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien1.Id, 
											Award__c = a1.Id);
		AwardLien__c al2 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien1.Id, 
											Award__c = a2.Id);
		AwardLien__c al3 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien2.Id, 
											Award__c = a2.Id);
		AwardLien__c al4 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien3.Id, 
											Award__c = a3.Id);
		AwardLien__c al5 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien4.Id, 
											Award__c = a4.Id);
		AwardLien__c al6 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien5.Id, 
											Award__c = a1.Id);
		AwardLien__c al7 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien6.Id, 
											Award__c = a2.Id);
		AwardLien__c al8 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien7.Id, 
											Award__c = a3.Id);
		AwardLien__c al9 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien3.Id, 
											Award__c = a1.Id);
		AwardLien__c al10 = new AwardLien__c(PercentToAward__c = 1.0, 
											 Lien__c = lien1.Id, 
											 Award__c = a5.Id);
		insert new list<AwardLien__c>{al1,al2,al3,al4,al5,al6,al7,al8,al9,al10};	
// end PD-566	
	}
	
	static testMethod void TestStageCalcs() {
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
						FROM Lien__c
						WHERE Submitted__c = false
						limit 1];
		lien.Stages__c = 'Submitted';
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
        lien.Date_Submitted__c = system.today().addDays(-15);
        update lien;
        lien.Date_Submitted__c = system.today().addDays(-30);
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
        lien.Date_Submitted__c = system.today().addDays(-45);
        lien.Date_Second_Sent__c = system.today().addDays(-15);
        update lien;
        lien.Date_Submitted__c = system.today().addDays(-60);
        lien.Date_Second_Sent__c = system.today().addDays(-30);
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
		lien.Date_Third_Sent__c = system.today().addDays(-10);
		update lien;
		lien.Stages__c = 'Audit';
		update lien;
        lien.Stages__c = 'Final';
        update lien;
        test.stopTest();
	}
// start PD-525/526
	static testMethod void TestFinalPayable() {
		test.startTest();
		list<Lien__c> liens = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, PayerType__c, 
									  Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
									  Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, RecordType.Name
							   FROM Lien__c
							   WHERE Submitted__c = true];
		for (Lien__c l : liens) {
	        l.Stages__c = 'Final';
	        l.Final_Payable__c = 5623.88;
		}
        update liens;
        test.stopTest();
	}
// end PD-525/526
	static testMethod void TestOutlyers() {
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, RecordType.Name, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c, PayerType__c
						FROM Lien__c
						WHERE Submitted__c = true
						limit 1];
		lien.RecordType.Name = 'PLRP Model';
        lien.Second_Notice_Sent__c = true;
        lien.Date_Second_Sent__c = system.today().addDays(-25);
        update lien;
        lien.Stages__c = 'Final';
        update lien;
        test.stopTest();
	}
// start PD-566
	static testMethod void TestFeeCreation1() {
		map<string,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
        Id rtId1 = rtMap.get('Medicaid').getRecordTypeId();
        Id rtId2 = rtMap.get('Medicare Global Model').getRecordTypeId();
        Id rtId3 = rtMap.get('Medicare Individual Resolution').getRecordTypeId();
        Id rtId4 = rtMap.get('Non-PLRP').getRecordTypeId();
        Id rtId5 = rtMap.get('PLRP Claim Detail').getRecordTypeId();
        Id rtId6 = rtMap.get('PLRP Model').getRecordTypeId();
        Id rtId7 = rtMap.get('Other Govt').getRecordTypeId();
		list<Lien__c> liens = [SELECT Id, RecordTypeId, Action__c, Account__c, Stages__c, Account__r.Lienholder_Contact__c, Substage__c, Claimant__c, PayerType__c,
									  (SELECT Id, Fee_Amount__c, Fee_Type__c, Fee_Subtype__c FROM Lien_Res_Fees__r),
									  (SELECT Id, Award__r.Settlement_Category__c, Award__r.Amount__c, Award__r.NetAmount__c, Award__r.Settlement_SubCategory__c, 
									 		  PercentToAward__c, PercentToLien__c 
									   FROM Awards__r)
							   FROM Lien__c
							   WHERE Submitted__c = true];
		test.startTest();
        for (Lien__c l : liens) {
        	l.Stages__c = 'Final';
        }
        liens[0].Override_Amount__c = 10000;
        liens[0].Date_Overridden__c = system.today();
        liens[0].OverrideReason__c = 'Because';
        liens[0].OverrideFLAmount__c = true;
        liens[0].RecordTypeId = rtId7;
        update liens;
        test.stopTest();
	}
	static testMethod void TestFeeCreation2() {
		map<string,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
        Id rtId1 = rtMap.get('Medicaid').getRecordTypeId();
        Id rtId2 = rtMap.get('Medicare Global Model').getRecordTypeId();
        Id rtId3 = rtMap.get('Medicare Individual Resolution').getRecordTypeId();
        Id rtId4 = rtMap.get('Non-PLRP').getRecordTypeId();
        Id rtId5 = rtMap.get('PLRP Claim Detail').getRecordTypeId();
        Id rtId6 = rtMap.get('PLRP Model').getRecordTypeId();
        Id rtId7 = rtMap.get('Other Govt').getRecordTypeId();
		Claimant__c c = [SELECT Action__c, Id, Law_Firm__c FROM Claimant__c WHERE Last_Name__c = 'RAPPER'];
        Fee_Model__c model = [SELECT MIR_Flat_Fee__c, NonP_Flat_Fee__c, OG_Flat_Fee__c, MGM_Flat_Fee__c, PLRP_Flat_Fee__c, M_M_Combo__c
							  FROM Fee_Model__c
							  WHERE Action__c = :c.Action__c];
		model.MIR_Flat_Fee__c = null;
		model.NonP_Flat_Fee__c = null;
		model.OG_Flat_Fee__c = null;
		model.MGM_Flat_Fee__c = null;
		model.PLRP_Flat_Fee__c = null;
		update Model;
        Lien__c lien1 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId1, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien2 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId2, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien3 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId3, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien4 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId4, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Asserted',
                                    Substage__c = 'No_Interest',
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien5 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId5, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien6 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId6, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
        Lien__c lien7 = new Lien__c(Account__c = c.Law_Firm__c, 
                                    Action__c = c.Action__c,
                                    RecordTypeId = rtId7, 
                                    Claimant__c = c.Id, 
                                    Cleared__c = false,
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Asserted',
                                    Substage__c = 'No_Interest',
                                    Eligible__c = 'Eligible_before_or_at_DOL',
                                    PayerType__c = 'Primary_Group_Health_Plan',
                                    State__c = 'CO', 
                                    Submitted__c = true);
		list<Lien__c> lienList = new list<Lien__c>{lien1,lien2,lien3,lien4,lien5,lien6,lien7};
		insert lienList;
		Award__c a1 = new Award__c(Claimant__c = c.Id, 
								   Action__c = c.Action__c,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #1',
								   Amount__c = 190000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a2 = new Award__c(Claimant__c = c.Id, 
								   Action__c = c.Action__c,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #2',
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a3 = new Award__c(Claimant__c = c.Id, 
								   Action__c = c.Action__c,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #3',
								   Amount__c = 200000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		Award__c a4 = new Award__c(Claimant__c = c.Id, 
								   Action__c = c.Action__c,
								   Settlement_Category__c = 'Base',
								   Settlement_SubCategory__c = 'Conserve_Claim',
								   Date_of_Award__c = system.today(), 
								   Description2__c = 'Award #4',
								   Amount__c = 150000, 
								   Distribution_Status__c = 'Not_Ready_to_be_Cleared');
		insert new list<Award__c>{a1,a2,a3,a4};
		AwardLien__c al1 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien1.Id, 
											Award__c = a1.Id);
		AwardLien__c al2 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien1.Id, 
											Award__c = a2.Id);
		AwardLien__c al3 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien2.Id, 
											Award__c = a2.Id);
		AwardLien__c al4 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien3.Id, 
											Award__c = a3.Id);
		AwardLien__c al5 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien4.Id, 
											Award__c = a4.Id);
		AwardLien__c al6 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien5.Id, 
											Award__c = a1.Id);
		AwardLien__c al7 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = lien6.Id, 
											Award__c = a2.Id);
		AwardLien__c al8 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien7.Id, 
											Award__c = a3.Id);
		AwardLien__c al9 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = lien4.Id, 
											Award__c = a4.Id);
		AwardLien__c al10 = new AwardLien__c(PercentToAward__c = 0.5, 
											 Lien__c = lien7.Id, 
											 Award__c = a4.Id);
		insert new list<AwardLien__c>{al1,al2,al3,al4,al5,al6,al7,al8,al9,al10};		
		test.startTest();
		lien4.Stages__c = 'No_Lien';
		lien7.Stages__c = 'No_Lien';
		update new list<Lien__c>{lien3,lien4,lien7};
		for (Lien__c l : lienList) {
			l.Stages__c = 'Final';
			l.Substage__c = 'Received_Final_Lien_Amount';
		}
		update lienList;
        Lien__c lien10 = new Lien__c(Account__c = c.Law_Firm__c, 
                                     Action__c = c.Action__c,
                                     RecordTypeId = rtId4, 
                                     Claimant__c = c.Id, 
                                     Cleared__c = false,
                                     Notes__c = 'This is a note', 
                                     Eligible__c = 'Eligible_before_or_at_DOL',
                                     PayerType__c = 'Primary_Group_Health_Plan',
                                     Stages__c = 'Submitted', 
                                     State__c = 'CO', 
                                     Submitted__c = true);
        Lien__c lien11 = new Lien__c(Account__c = c.Law_Firm__c, 
                                     Action__c = c.Action__c,
                                     RecordTypeId = rtId7, 
                                     Claimant__c = c.Id, 
                                     Cleared__c = false,
                                     Notes__c = 'This is a note', 
                                     Stages__c = 'Submitted', 
                                     Eligible__c = 'Eligible_before_or_at_DOL',
                                     PayerType__c = 'Primary_Group_Health_Plan',
                                     State__c = 'CO', 
                                     Submitted__c = true);
        Lien__c lien12 = new Lien__c(Account__c = c.Law_Firm__c, 
                                     Action__c = c.Action__c,
                                     RecordTypeId = rtId1, 
                                     Claimant__c = c.Id, 
                                     Cleared__c = false,
                                     Notes__c = 'This is a note', 
                                     Stages__c = 'Submitted', 
                                     Eligible__c = 'Eligible_before_or_at_DOL',
                                     PayerType__c = 'Primary_Group_Health_Plan',
                                     State__c = 'CO', 
                                     Submitted__c = true);
        insert new list<Lien__c>{lien10,lien11,lien12};
		AwardLien__c al11 = new AwardLien__c(PercentToAward__c = 0.5, 
											 Lien__c = lien10.Id, 
											 Award__c = a4.Id);
		AwardLien__c al12 = new AwardLien__c(PercentToAward__c = 0.5, 
											 Lien__c = lien11.Id, 
											 Award__c = a4.Id);
		AwardLien__c al13 = new AwardLien__c(PercentToAward__c = 1.0, 
											 Lien__c = lien12.Id, 
											 Award__c = a4.Id);
		insert new list<AwardLien__c>{al11,al12,al13};
		lien10.Stages__c = 'Final';
		lien11.Stages__c = 'Final';
		lien12.Stages__c = 'Final';
		update new list<Lien__c>{lien10,lien11,lien12};
        test.stopTest();
	}
// end PD-566
}