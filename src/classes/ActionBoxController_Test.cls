/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of ActionBoxControllerExtension

History
	Updated: lmsw - March 2017 for March Release
	Purpose: Add test functions for ClaimantBoxControllerExtension

	Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claimant__c.Address_1__c

    Updated: lmsw - March 2017
    Purpose: PRODSUPT-57 Remove Lien.Lien_Type__c
    		 					  
    Created: RAP - April 2017 for May Release
    Purpose: Repair testMethod
    		 Coverage as of 4/25 - ActionBoxControllerExtension: 100%
    		 					   ClaimantBoxControllerExtension: 100%

*/
@isTest
private class ActionBoxController_Test {
    
@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Id rtId_lien = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicaid').getRecordTypeId();
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = false);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Claimant__c c = new Claimant__c(Address_1__c = '123 Main St',
										City__c = 'Denver',
										Email__c = 'rap@rap.com',
										Law_Firm__c = acc.Id,
										First_Name__c = 'RAP',
										Last_Name__c = 'RAPPER',
										Phone__c = '3035551212',
										SSN__c = '135461448',
										State__c = 'CO',
										Status__c = 'Not_Ready_to_be_Cleared',
										Zip__c = '80138');
        insert c;
        Lien__c l = new Lien__c(Name__c = 'Test Lien',
        						RecordTypeId = rtId_lien,
        						Account__c = acc.Id,
        						Stages__c = 'To Be Submitted',
        						Claimant__c = c.Id,
        						Action__c = action.Id);
        insert l;
        system.debug('Lien: ' + l);
	}
	
	static testMethod void TestExtension() {
		Claimant__c c = [SELECT Id FROM Claimant__c WHERE Last_Name__c = 'RAPPER' limit 1];
		Lien__c l = [SELECT Id FROM Lien__c limit 1];
		test.startTest();
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(c);
		ActionBoxControllerExtension ctrl = new ActionBoxControllerExtension(stdCtrl);
		ApexPages.currentPage().getParameters().put('sidtp', 'p1');
		ctrl = new ActionBoxControllerExtension(stdCtrl);
		
		ApexPages.StandardController stdCtrl_lien = new ApexPages.StandardController(l);
		ClaimantBoxControllerExtension cctrl = new ClaimantBoxControllerExtension(stdCtrl_lien);
		ApexPages.currentPage().getParameters().put('isdtp','p1');
		cctrl = new ClaimantBoxControllerExtension(stdCtrl_lien); 
		test.stopTest();
	}
}