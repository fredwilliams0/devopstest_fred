/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: PD-524/525/526/527 - Automation - Handler for Account trigger events - first pass - create Action_Accounts after insert.

*/

public with sharing class AccountTriggerHandler {

    public void onAfterInsert(list<Account> newAccts, map<Id, Account> newMap) {
   		CreateActionAccounts(newMap.keySet());
	}
 
//	public void onBeforeUpdate(list<Account> newActionRecs, map<Id, Account> newActionsMap, map<Id, Account> oldActionsMap) {
//	}

//	public void onBeforeInsert(list<Account> newActionRecs, map<Id, Account> newActionsMap) {
//	}

    public void onAfterUpdate(list<Account> newAccts, map<Id, Account> oldMap) {
   		CreateActionAccounts(oldMap.keySet());
	}

	private static void CreateActionAccounts(set<Id> aIds) {
    	list<Action_Account__c> aaList = [SELECT Account__c, Action__c FROM Action_Account__c WHERE Account__c IN :aIds];
    	map<Id, set<Id>> aaMap = new map<Id,set<Id>>();
    	for (Action_Account__c aa : aaList) {
    		if (aaMap.containsKey(aa.Account__c))
    			aaMap.get(aa.Account__c).add(aa.Action__c);
    		else
    			aaMap.put(aa.Account__c, new set<Id>{aa.Action__c});
    	}
		Id actionId = ActionAssignment2__c.getInstance(userInfo.getUserId()).Action__c;
		list<Action_Account__c> insertList = new list<Action_Account__c>();
		for (Id aId : aIds) {
			if (!aaMap.containsKey(aId) || !aaMap.get(aId).contains(actionId)) {
				Action_Account__c aa = new Action_Account__c(Action__c = actionId,
															 Account__c = aId);
				insertList.add(aa);
			}
		}
		if (!insertList.isEmpty())
			insert insertList;
	}
}