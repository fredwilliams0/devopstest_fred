/*
    Created - RAP February 2017 for March Release
    Purpose - PD-431 - Release Dates Auto-Move (Batch processor grabs all Awards belonging to an Action with a Distribution 
    		  Date of the day it runs, collects amounts available for disbursement, and moves those funds out of availability
History:

    Updated: RAP - May 2017 for May15 Release
    Purpose: PD-571 - Quality Control implementation - 12 causes for no distribution
    
*/
global class DistributionBatch implements Database.Batchable<Award__c>, Database.Stateful {

	public final static set<string> SUBROSTATES = new set<string>{'NY', 'NJ', 'VA', 'MO', 'CT', 'AZ', 'KS', 'NC'};
    public Date queryDate{get;set;}
	public string action{get;set;}
	public map<Id,Claimant__c> cMap{get;set;}
	public map<Id,list<Model_Detail__c>> modelMap{get;set;}
	public map<Id,Lien__c> ledMap{get;set;}
	public integer numReleases{get;set;}
    public DistributionBatch(string act) {
    	queryDate = system.today();
		numReleases = 0;
    	action = act;
    }
    
	global Iterable<Award__c> start(Database.BatchableContext BC) {
		DisbursementDates__c ddc = DisbursementDates__c.getInstance(action);
		if (ddc == null || ddc.Date__c != queryDate) return new list<Award__c>();
		set<Id> cSet = new set<Id>();
		list<Id> mList = new list<Id>();
		list<Id> lList = new list<Id>();
		list<Award__c> awards = [SELECT Amount__c, Assoc_Costs_Award_Deductions__c, Id, Amount_Applied__c, Amount_Disbursed__c, 
										Claimant__r.DOB__c,	Name, Claimant__c, Claimant__r.Total_Applied__c, Claimant__r.Num_Liens__c, 
										Claimant__r.Negative_Balance__c, Claimant__r.Date_SSN_Changed__c, Description2__c,
									   (SELECT Lien__c, Lien__r.HBAmount__c, Lien__r.Total_Fees_Applied__c, Lien__r.Total_Fees_Paid__c, 
									   		   Lien__r.Name__c, Lien__r.RecordType.Name, Lien__r.Stages__c, Lien__r.Substage__c, 
									   		   Lien__r.Final_Payable__c, Lien__r.PayerType__c, Lien__r.Account__c, Lien__r.Date_No_Interest__c, 
									   		   Lien__r.Subrogation_Rights__c, Lien__r.State__c, Lien__r.Date_Submitted__c
									    FROM Liens__r)
								FROM Award__c
								WHERE Action__r.Name = :action
								AND Claimant__r.Negative_Balance__c = false 	// #1
								AND Claimant__r.Num_Liens__c > 0				// #2
								AND Claimant__r.Total_Applied__c > 0			// #3
								AND (Distribution_Status__c = 'Cleared'
								OR Distribution_Status__c = 'Released_to_a_Holdback')];
		system.debug(loggingLevel.INFO, 'RAP --->> awards = ' + awards);
		for (Award__c aw : awards) {
			cSet.add(aw.Claimant__c);
			for (AwardLien__c al : aw.Liens__r) {
				mList.add(al.Lien__r.Account__c);
				lList.add(al.Lien__c);
			}
		}
		map<Id,Claimant__c> claimants = new map<Id,Claimant__c>([SELECT Id, DOB__c, Date_SSN_Changed__c,
																		(SELECT Id, PLC_Determination__c FROM HealthPlans__r)
																 FROM Claimant__c
																 WHERE Id in :cSet]);
		cMap = new map<Id,Claimant__c>();
		for (Award__c a : awards)
			cMap.put(a.Id, claimants.get(a.Claimant__c));
		list<Model_Detail__c> modelList = [SELECT Payable_Amount__c, Name, Eligibility__c, Award_Type__c, Provider__c, RecordTypeId, PayerType__c
										   FROM Model_Detail__c
										   WHERE Action__r.Name = :action
										   AND Provider__c IN :mList];
		modelMap = new map<Id,list<Model_Detail__c>>();
		for (Model_Detail__c md : modelList) {
			if (modelMap.containsKey(md.Provider__c))
				modelMap.get(md.Provider__c).add(md);
			else
				modelMap.put(md.Provider__c, new list<Model_Detail__c>{md});
		}
		ledMap = new map<Id,Lien__c>([SELECT Id, Name, RecordType.Name, Non_PLRP_Type__c,
											 (SELECT Date_Start__c, Date_End__c, LienType__c FROM LienEligibilityDates__r),
											 (SELECT Injury__r.DOL__c FROM Injuries__r)
									  FROM Lien__c
									  WHERE Id IN :lList]);
		
		return awards;
	}

	public void execute(Database.BatchableContext BC, list<Award__c> awards) {
		if (awards == null || awards.isEmpty())	return;
		list<Release__c> releases = new list<Release__c>();
		for (Award__c a : awards) {
system.debug(loggingLevel.INFO, 'RAP --->> Award = ' + a.Description2__c);
			Claimant__c claimant = cMap.get(a.Id);
system.debug(loggingLevel.INFO, 'RAP --->> Health Plan = ' + claimant.HealthPlans__r);
			integer plrp = 0;
			decimal liens = 0.00;
			boolean skip = false;
			for (AwardLien__c al : a.Liens__r) {
				Lien__c lien = ledMap.get(al.Lien__c);
				for (InjuryLien__c ilc : lien.Injuries__r) {
					for (LienEligibilityDate__c led : lien.LienEligibilityDates__r) {									// #10
						if (led.LienType__c == 'PartC' &&
							led.Date_Start__c < ilc.Injury__r.DOL__c && 
							led.Date_End__c > ilc.Injury__r.DOL__c &&
							lien.Non_PLRP_Type__c != 'Part_C') {
							skip = true;
							break;
						}
					}
				}
				liens += al.Lien__r.Final_Payable__c;
				if (claimant.DOB__c.monthsBetween(queryDate) >= 780 && al.Lien__r.RecordType.Name == 'Medicare Global Model' && 
					al.Lien__r.Stages__c == 'No_Lien' && al.Lien__r.Substage__c == 'Not_Eligible') {					// #4
					skip = true;
				}
				if (al.Lien__r.PayerType__c != null && al.Lien__r.PayerType__c.startsWith('Primary')) {
					for (Model_Detail__c md : modelMap.get(al.Lien__r.Account__c)) {
						if (md.PayerType__c == al.Lien__r.PayerType__c && md.Payable_Amount__c > al.Lien__r.Final_Payable__c) {	// #6
							skip = true;
							break;
						}
					}
					if (al.Lien__r.RecordType.Name.startsWith('PLRP')) {
						if (al.Lien__r.Stages__c == 'No_Lien' && al.Lien__r.Date_No_Interest__c == null) {				// #11
							skip = true;
						}
						if (plrp == 1) {																				// #7
							skip = true;
						}
						plrp = 1;
					}
				}
				if (claimant.Date_SSN_Changed__c != null && claimant.Date_SSN_Changed__c > al.Lien__r.Date_Submitted__c) { // #12
					skip = true;
				}
				if (SUBROSTATES.contains(al.Lien__r.State__c) && string.isBlank(al.Lien__r.Subrogation_Rights__c)) {	// #9
					skip = true;
				}
			}
			if (liens > a.Amount__c*0.33) {										// #8
				skip = true;
			}
			for (Health_Plan__c hp : claimant.HealthPlans__r) {
				if (string.isBlank(hp.PLC_Determination__c)) {					// #5
					skip = true;
				}
			}
			if (skip) continue;
			decimal hold = a.Amount_Applied__c + a.Assoc_Costs_Award_Deductions__c + a.Amount_Disbursed__c;
			for (AwardLien__c al : a.Liens__r) {
				decimal unpaid = al.Lien__r.HBAmount__c + al.Lien__r.Total_Fees_Applied__c - al.Lien__r.Total_Fees_Paid__c;
				hold += unpaid;
			}
			if (a.Amount__c > hold) {
				decimal sendAmt = a.Amount__c - hold;
				Release__c send = new Release__c(Award__c = a.Id,
												 Amount__c = sendAmt,
												 Date__c = queryDate);
				releases.add(send);
			}
		}
		if (!releases.isEmpty()) {
			insert releases;
			numReleases += releases.size();
		}
	}
	
	public void finish(Database.BatchableContext BC) {
		system.debug(loggingLevel.INFO, 'RAP --->> Number of Releases created: ' + numReleases);
	}
}