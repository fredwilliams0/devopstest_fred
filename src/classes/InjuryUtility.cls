/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries

*/
public without sharing class InjuryUtility { 

	public static void NameInjuries(list<Injury__c> injuries) { 	   
	    if (trigger.isBefore && trigger.isInsert) {
	    	list<Id> cList = new list<Id>();
	    	for (Injury__c i : injuries)
	    		cList.add(i.Claimant__c);
		    map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c WHERE Id IN :cList]);
			for (Injury__c i : injuries)
				i.Name = cMap.get(i.Claimant__c).Name + ' - ' + i.Injury_Description__c;
	    }
	}    
}