/*
    Created - RAP May 2017 for May22 Patch
    Purpose - Creation of AwardLien junctions given InjuryLiens and InjuryAwards
    
*/
global class AwardLienBatch implements Database.Batchable<SObject>, Database.Stateful {

	public static final string QUERY = 'SELECT Award__c, Id, Injury__c FROM InjuryAward__c WHERE Award__r.Action__r.Name = ';
	public map<Id,set<Id>> iaMap{get;set;}
	public map<Id,set<Id>> ilMap{get;set;}
	public map<Id,map<Id, AwardLien__c>> alMap2{get;set;}
	public map<Id,set<Id>> alMap{get;set;} // multiple liens per award
	public map<Id,set<Id>> laMap{get;set;} // multiple awards per lien
	public string action{get;set;}
	
    global AwardLienBatch(string act) {
    	action = act;
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		iaMap = new map<Id,set<Id>>();
		ilMap = new map<Id,set<Id>>();
		alMap = new map<Id,set<Id>>();
		laMap = new map<Id,set<Id>>();
		alMap2 = new map<Id,map<Id,AwardLien__c>>();
		string a = action;
		list<InjuryLien__c> ilList = [SELECT Injury__c, Lien__c, Id 
									  FROM InjuryLien__c 
									  WHERE Lien__r.Action__r.Name = :action
									  AND Lien__r.Stages__c != 'No_Lien'];
		for (InjuryLien__c il : ilList) {
			if (ilMap.containsKey(il.Injury__c))
				ilMap.get(il.Injury__c).add(il.Lien__c);
			else
				ilMap.put(il.Injury__c, new set<Id>{il.Lien__c});
		}
		list<AwardLien__c> alList = [SELECT PercentToLien__c, PercentToAward__c, Award__c, Id, Lien__c
									 FROM AwardLien__c
									 WHERE Lien__r.Action__r.Name = :action
									 AND Lien__r.Stages__c != 'No_Lien'];
		for (AwardLien__c al : alList) {
			if (alMap.containsKey(al.Award__c)) {
				alMap.get(al.Award__c).add(al.Lien__c);
				alMap2.get(al.Award__c).put(al.Lien__c, al);
			}
			else {
				alMap.put(al.Award__c, new set<Id>{al.Lien__c});
				alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
			}
		}
		list<InjuryAward__c> iaList = [SELECT Award__c, Id, Injury__c FROM InjuryAward__c WHERE Award__r.Action__r.Name = :action];
		for (InjuryAward__c ia : iaList) {
			if (iaMap.containsKey(ia.Award__c))
				iaMap.get(ia.Award__c).add(ia.Injury__c);
			else
				iaMap.put(ia.Award__c, new set<Id>{ia.Injury__c});
		}
		return Database.getQueryLocator(QUERY + '\'' + a + '\'');
	}

	public void execute(Database.BatchableContext BC, list<InjuryAward__c> injuryAwards) {
		map<Id,set<AwardLien__c>> upsertMap = new map<Id,set<AwardLien__c>>();
		list<AwardLien__c> upsertList = new list<AwardLien__c>();
		set<AwardLien__c> upsertSet = new set<AwardLien__c>();
		for (Id aId : iaMap.keySet()) {
			for (Id iId : iaMap.get(aId)) { // loop through InjuryAwards by award
				if (ilMap.containsKey(iId)) { // if we have a corresponding InjuryLien
					for (Id lId : ilMap.get(iId)) { // loop through InjuryLiens by injury
						AwardLien__c al;
						if (!alMap.containsKey(aId) || !alMap.get(aId).contains(lId)) {
							al = new AwardLien__c(Award__c = aId,
												  Lien__c = lId);
						}
						else
							al = alMap2.get(aId).get(lId);

						if (upsertMap.containsKey(aId))
							upsertMap.get(aId).add(al);
						else 
							upsertMap.put(aId, new set<AwardLien__c>{al});
						if (alMap.containsKey(al.Award__c)) {
							alMap.get(al.Award__c).add(al.Lien__c);
							alMap2.get(al.Award__c).put(al.Lien__c, al);
						}
						else {
							alMap.put(al.Award__c, new set<Id>{al.Lien__c});
							alMap2.put(al.Award__c, new map<Id,AwardLien__c>{al.Lien__c => al});
						}
						if (laMap.containsKey(al.Lien__c))
							laMap.get(al.Lien__c).add(al.Award__c);
						else
							laMap.put(al.Lien__c, new set<Id>{al.Award__c});
					}
				}
			}
		}
		for (AwardLien__c al : upsertSet) { // calculate 1 lien --> multiple award percentages 
			set<Id> lls = laMap.get(al.Lien__c);
			decimal percent2 = (1/decimal.valueOf(lls.size()));
			percent2 = percent2.setScale(2);
			al.PercentToAward__c = percent2;
		}
		if (!upsertSet.isEmpty()) {
			upsertList.addAll(upsertSet);
			upsert upsertList;
		}
	}
	
	public void finish(Database.BatchableContext BC) {

	}
}