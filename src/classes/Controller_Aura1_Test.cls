/*
    Created: RAP - January 2017 for first release
    Purpose: test function of Controller_Aura1

    History:
    
    Updated: Lmsw - January 2017 for March Release
    Purpose: add test coverage for claimant, claimantStatusPicklist, andupdateClaimant
    
    Updated: RAP - March 2017 for March Release
    Purpose: test completion/refactor.
    
    Updated: lmsw - March 2017
    Purpose: Change Claimant__c.Address__c to Claimant__c.Address_1__c
    
    Updated: RAP - March 2017 for March Patch
    Purpose: PD-440 - Action - Lien Tab - Alert Component
    		 PD-464 - Action - Lien Tab - Lien Inventory Component
    		 PRODSUPT-57 - Lien__c.Lien Type - Is it REALLY necessary?
		     Coverage as of 3/21/17 - Controller_Aura1: 95%
		                            - LienUtility: 94%
                           			- Lien Trigger: 100%
*/
@isTest
private class Controller_Aura1_Test {

@testSetup
    static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
    	Id lrtId = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('Medicaid').getRecordTypeId();
		Id pId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;
		User u = new User(UserName = 'TestUser@structures.com',
						  isActive = true,
						  ProfileId = pId,
						  LastName = 'User', 
						  Email = 'rap@rap.com', 
						  Alias = 'tuser', 
						  TimeZoneSidKey = 'America/Denver', 
						  LocaleSidKey = 'En_US', 
						  EmailEncodingKey = 'ISO-8859-1', 
						  LanguageLocaleKey = 'en_US');
		insert u;
		StageSubstageMap__c ssMap = new StageSubstageMap__c(Name = 'Third_Notice_Sent',
															Stage__c = 'Submitted',
															LienTypes__c = 'Medicaid');
		insert ssMap;
		Filters__c filters = new Filters__c(Name='Alert3',
											FilterId__c = '00BV0000001BMknMAG');
		insert filters;
		system.runAs(u) {
	        Action__c action = new Action__c(Name = 'Test Action',
	                                         Active__c = true);
	        insert action;
			ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
																Action__c = action.Id);
			insert aac;
	        Account a = new Account(Name = 'Law Firm',
	        						RecordTypeId = rtId,
	        						Priority_Firm__c = true, 
	                                BillingStreet = '123 Main St',
	                                BillingCity = 'Denver',
	                                BillingState = 'CO',
	                                BillingPostalCode = '80202');
	        Account b = new Account(Name = 'Payable Entity',
	                                BillingStreet = '345 Elm St',
	                                BillingCity = 'Portland',
	                                BillingState = 'OR',
	                                BillingPostalCode = '50303');
	        insert new list<Account>{a,b};
	        Account a1 = new Account(Name = 'Referring Law Firm',
	        						 RecordTypeId = rtId,
	        						 Lead_Law_Firm__c = a.Id, 
	                                 BillingStreet = '124 Main St',
	                                 BillingCity = 'Denver',
	                                 BillingState = 'CO',
	                                 BillingPostalCode = '80202');
			insert a1;
	        Contact c = new Contact(LastName = 'Crain',
	                                email = 'denny@crain.com');
	        insert c;
			Time_Processing__c tpc = new Time_Processing__c(Name = action.Id,
															Second_Notice_Interval__c = 30,
															Third_Notice_Interval__c = 45);
			insert tpc;
	        Claimant__c c1 = new Claimant__c(Address_1__c = '123 Main St',
											 City__c = 'Denver',
											 Email__c = 'rap@rap.com',
											 Law_Firm__c = a.Id,
											 First_Name__c = 'RAP',
											 Last_Name__c = 'RAP',
											 Phone__c = '3035551212',
											 SSN__c = '135461448',
											 State__c = 'CO',
											 Status__c = 'Not_Ready_to_be_Cleared',
											 Zip__c = '80138');
	        Claimant__c c2 = new Claimant__c(Address_1__c = '123 Elm St',
											 City__c = 'Denver',
											 Email__c = 'rap@rap.com',
											 Law_Firm__c = a1.Id,
											 First_Name__c = 'RAP',
											 Last_Name__c = 'RAP',
											 Phone__c = '3035551212',
											 SSN__c = '135461449',
											 State__c = 'CO',
											 Status__c = 'Cleared',
											 Zip__c = '80138');
	        Claimant__c c3 = new Claimant__c(Address_1__c = '123 Maple St',
											 City__c = 'Denver',
											 Email__c = 'rap@rap.com',
											 Law_Firm__c = a1.Id,
											 First_Name__c = 'RAP',
											 Last_Name__c = 'RAP',
											 Phone__c = '3035551212',
											 SSN__c = '135461450',
											 State__c = 'CO',
											 Status__c = 'Cleared',
											 Zip__c = '80138');
	        insert new list<Claimant__c>{c1,c2,c3};
	        Id aId = action.Id;
	        Action_Account__c aa1 = new Action_Account__c(Action__c = aId,
	        											  Account__c = a.Id);
	        Action_Account__c aa2 = new Action_Account__c(Action__c = aId,
	        											  Account__c = b.Id);
	        insert new list<Action_Account__c>{aa1,aa2};
	        Lien__c lien1 = new Lien__c(Account__c = a.Id, 
	                                    Action__c = aId, 
	                                    Claimant__c = c1.Id, 
	                                    Cleared__c = true,
	                                    Date_Submitted__c = system.today(), 
	                                    RecordTypeId = lrtId, 
	                                    Notes__c = 'This is a note', 
	                                    Payable_Entity__c = b.Id,
	                                    Stages__c = 'Final',
	                                    Substage__c = 'Paid', 
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        Lien__c lien2 = new Lien__c(Account__c = a.Id, 
	                                    Action__c = aId, 
	                                    Claimant__c = c3.Id, 
	                                    Cleared__c = true,
	                                    Date_Submitted__c = system.today().addDays(-30), 
	                                    RecordTypeId = lrtId, 
	                                    Notes__c = 'This is also a note', 
	                                    Payable_Entity__c = b.Id,
	                                    Stages__c = 'Final', 
	                                    Substage__c = 'Terms Applied',
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        Lien__c lien3 = new Lien__c(Account__c = a.Id, 
	                                    Action__c = aId, 
	                                    Claimant__c = c3.Id, 
	                                    Cleared__c = false,
	                                    Date_Submitted__c = system.today().addDays(-30), 
	                                    RecordTypeId = lrtId, 
	                                    Notes__c = 'This is also a note', 
	                                    Payable_Entity__c = b.Id,
	                                    Stages__c = 'To_Be_Submitted', 
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        insert new list<Lien__c>{lien1,lien2,lien3};
	        list<Lien_Negotiated_Amounts__c> lienList = new list<Lien_Negotiated_Amounts__c>();
	        list<LienEligibilityDate__c> ledList = new list<LienEligibilityDate__c>();
	        list<Injury__c> injList = new list<Injury__c>();
	        for (integer j=0; j<4; j++) {
				Injury__c inj1 = new Injury__c(Compensable__c = 'True', 
	        								   Action__c = action.Id,
	        								   Claimant__c = j<2 ? c1.Id : c3.Id, 
	        								   Injury_Category__c = string.valueOf(j), 
	        								   Injury_Description__c = 'Injury Most Foul ' + string.valueOf(j), 
											   DOL__c = system.today().addDays(-60));
	        	injList.add(inj1);
	        }
	        insert injList;
	        Lien_Negotiated_Amounts__c lnac = new Lien_Negotiated_Amounts__c(Lien__c = lien2.Id, 
	                                                                         Lien_Amount__c = 500000, 
	                                                                         Lien_Amount_Date__c = system.today(), 
	                                                                         Phase__c = 'Final');
	        insert lnac;
			LienEligibilityDate__c led = new LienEligibilityDate__c(Date_End__c = system.today().addDays(30),
	       															Date_Start__c = system.today().addDays(-30),
	       															Lien__c = lien3.Id, 
	       															LienType__c = 'Part A');
	       	insert led;
	       	InjuryLien__c il1 = new InjuryLien__c(Injury__c = injList[0].Id,
	        									  Lien__c = lien1.Id);
	       	InjuryLien__c il2 = new InjuryLien__c(Injury__c = injList[1].Id,
	        									  Lien__c = lien1.Id);
	       	InjuryLien__c il3 = new InjuryLien__c(Injury__c = injList[2].Id,
	        									  Lien__c = lien2.Id);
	       	InjuryLien__c il4 = new InjuryLien__c(Injury__c = injList[3].Id,
	        									  Lien__c = lien2.Id);
	        insert new list<InjuryLien__c>{il1,il2,il3,il4};
	        Award__c aw1 = new Award__c(Action__c = aId, 
										Claimant__c = c1.Id, 
									    Settlement_Category__c = 'Base',
									    Settlement_SubCategory__c = 'Conserve_Claim',
										Date_of_Award__c = system.today(), 
										Amount__c = 75000, 
										Description__c = 'Description');
	        Award__c aw2 = new Award__c(Action__c = aId, 
										Claimant__c = c2.Id, 
									    Settlement_Category__c = 'Base',
									    Settlement_SubCategory__c = 'Conserve_Claim',
										Date_of_Award__c = system.today(), 
										Amount__c = 170000, 
										Description__c = 'Description');
	        Award__c aw3 = new Award__c(Action__c = aId, 
										Claimant__c = c3.Id, 
									    Settlement_Category__c = 'Base',
									    Settlement_SubCategory__c = 'Conserve_Claim',
										Date_of_Award__c = system.today(), 
										Amount__c = 175000, 
										Description__c = 'Description');
	        insert new list<Award__c>{aw1,aw2,aw3};
	        Release__c rel = new Release__c(Award__c = aw1.Id);
	        insert rel;
	        AwardLien__c al1 = new AwardLien__c(Award__c = aw1.Id,
	        									Lien__c = lien1.Id);
	        AwardLien__c al2 = new AwardLien__c(Award__c = aw2.Id,
	        									Lien__c = lien2.Id);
	        AwardLien__c al3 = new AwardLien__c(Award__c = aw3.Id,
	        									Lien__c = lien2.Id);
	        insert new list<AwardLien__c>{al1,al2,al3};
	        InjuryAward__c ia1 = new InjuryAward__c(Injury__c = injList[0].Id,
	        										Award__c = aw1.Id);
	        InjuryAward__c ia2 = new InjuryAward__c(Injury__c = injList[1].Id,
	        										Award__c = aw1.Id);
	        InjuryAward__c ia3 = new InjuryAward__c(Injury__c = injList[0].Id,
	        										Award__c = aw2.Id);
	        InjuryAward__c ia4 = new InjuryAward__c(Injury__c = injList[1].Id,
	        										Award__c = aw2.Id);
	        insert new list<InjuryAward__c>{ia1,ia2,ia3,ia4};
		}
    }
    static testMethod void TestLiens() {
        User u = [SELECT Id, Name FROM User WHERE LastName = 'User' limit 1];
        test.startTest();
		system.runAs(u) {
	        Lien__c l = [SELECT Id, Account__c, Action__c, Claimant__c, Ledger__c, Payable_Entity__c, RecordTypeId 
	                     FROM Lien__c
	                     WHERE Stages__c = 'To_Be_Submitted' 
	                     limit 1];
	        Lien__c lCheck = Controller_Aura1.getLien(l.Id);
	        list<Lien__c> checkLiens = Controller_Aura1.getLiens(string.valueOf(l.Claimant__c), string.valueOf(l.Action__c)); 
	        list<Injury__c> iList = Controller_Aura1.getInjuries(l.Id);
	        list<Award__c> aList = Controller_Aura1.getAwards(l.Id);
	        map<string,list<LienEligibilityDate__c>> ledMap = Controller_Aura1.getLienEligibleDates(l.Id);
	        list<Claimant__c> cList = Controller_Aura1.getClaimants(string.valueOf(l.Action__c));
	        list<Lien_Negotiated_Amounts__c> l1 = Controller_Aura1.getlienAmtList(l.Id); 
	        map<string,list<Lien__c>> lienMap = Controller_Aura1.getLienMap(string.valueOf(l.Action__c));
	        lienMap = Controller_Aura1.getLienMap2(string.valueOf(l.Claimant__c));
	        map<string,string> lienTypeMap = Controller_Aura1.getLienTypeMap();
	        map<string,string> lienStageMap = Controller_Aura1.getLienStageMap();
	        map<string,string> lienSubStageMap = Controller_Aura1.getLienSubstageMap();
	// bump coverage for LienUtility
	        Lien__c lien3 = new Lien__c(Account__c = l.Account__c, 
	                                    Action__c = l.Action__c, 
	                                    Claimant__c = l.Claimant__c, 
	                                    Cleared__c = false,
	                                    Date_Submitted__c = system.today().addDays(-30), 
	                                    Ledger__c = l.Ledger__c, 
	                                    RecordTypeId = l.RecordTypeId, 
	                                    Notes__c = 'This is a note', 
	                                    Payable_Entity__c = l.Payable_Entity__c,
	                                    Stages__c = 'Submitted', 
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        Lien__c lien4 = new Lien__c(Account__c = l.Account__c, 
	                                    Action__c = l.Action__c, 
	                                    Claimant__c = l.Claimant__c, 
	                                    Cleared__c = false,
	                                    Date_Submitted__c = system.today().addDays(-75), 
	                                    Ledger__c = l.Ledger__c, 
	                                    RecordTypeId = l.RecordTypeId, 
	                                    Notes__c = 'This is a note', 
	                                    Payable_Entity__c = l.Payable_Entity__c,
	                                    Stages__c = 'Submitted',
	                                    Substage__c = 'Second_Notice_Sent',
	                                    Second_Notice_Sent__c = true, 
	                                    Date_Second_Sent__c = system.today().addDays(-45),
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        Lien__c lien5 = new Lien__c(Account__c = l.Account__c, 
	                                    Action__c = l.Action__c, 
	                                    Claimant__c = l.Claimant__c, 
	                                    Cleared__c = false,
	                                    Date_Submitted__c = system.today().addDays(-105), 
	                                    Ledger__c = l.Ledger__c, 
	                                    RecordTypeId = l.RecordTypeId, 
	                                    Notes__c = 'This is a note', 
	                                    Payable_Entity__c = l.Payable_Entity__c,
	                                    Stages__c = 'Submitted',
	                                    Second_Notice_Sent__c = true,
	                                    Third_Notice_Sent__c = true, 
	                                    Substage__c = 'Third_Notice_Sent',
	                                    Date_Second_Sent__c = system.today().addDays(-75),
	                                    Date_Third_Sent__c = system.today().addDays(-45),
	                                    State__c = 'CO', 
	                                    Submitted__c = true);
	        insert new list<Lien__c>{lien3,lien4,lien5};
			l.Stages__c = 'Submitted';
			update new list<Lien__c>{l,lien3,lien4,lien5};
	//
			Controller_Aura1.updateLienCard('Audit', lien3.Id);
			lien3 = Controller_Aura1.updateLien(lien3.Id, 'Reason', 15456.73);
		}
        test.stopTest();
    }
// Test Claimant    
    static testMethod void TestClaimant() {
        test.startTest();
        Claimant__c c = [SELECT Id, name, email__c, Phone__c, SSN__c, status__c, Law_Firm__c
                         FROM Claimant__c
                         limit 1];
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(c);
        Controller_Aura1 ctrl = new Controller_Aura1(stdctrl);
        Claimant__c cCheck = Controller_Aura1.getClaimant(c.Id);
        system.assertEquals('RAP RAP',[SELECT Id, Name FROM Claimant__c WHERE Id =:c.Id].Name);
        
        List<String> statusList = Controller_Aura1.getClaimantStatusPicklist();
        system.assertEquals(3,statusList.size());

        Controller_Aura1.updateClaimant(cCheck.id, 'Holdback');
        system.assertEquals('Holdback', [SELECT id, status__c FROM Claimant__c WHERE Id =: cCheck.id].status__c);
        test.stopTest();
    }
// Test Injuries and Awards    
    static testMethod void TestInjuriesAndAwards() {
        test.startTest();
        User u = [SELECT Id, Name FROM User WHERE LastName = 'User' limit 1];
        
        //start MF addition
        Action__c theAction = [SELECT Id FROM Action__c where name = 'Test Action' LIMIT 1];//could move getaction method below but did not want to take it out of scope of runas - MF
        ActionAssignment2__c actas = new ActionAssignment2__c();
        actas.name = u.Id;
        actas.Action__c = theAction.Id;
        insert actas;
        //end MF addition (once more change below)
        
        list<Lien__c> l = [SELECT Id, Account__c, Action__c, Claimant__c, Ledger__c, Payable_Entity__c, Claimant__r.Name, 
								  Claimant__r.Email__c, Claimant__r.Phone__c, Claimant__r.SSN__c, Claimant__r.Status__c, 
								  Claimant__r.Law_Firm__c 
		                   FROM Lien__c];
        list<Injury__c> injList = Controller_Aura1.getInjuries2(l[1].Claimant__c);
        list<Award__c> awList = Controller_Aura1.getAwardsByLien(l[1].Id);
        Claimant__c claimant = Controller_Aura1.updateClaimant(l[1].Claimant__c, 'Cleared');
        list<Action__c> actionList = Controller_Aura1.getActions(); // moved from below
        system.runAs(u) {
            Action__c action = Controller_Aura1.getAction(actionList[0].Id);
			list<string> actions = Controller_Aura1.getAction2();
			list<string> alerts = Controller_Aura1.getAlerts();
        }        
        list<string> stages = Controller_Aura1.getStages();
		map<string,string> stageMap = Controller_Aura1.getStagesMap();
        list<Lien__c> lienList = Controller_Aura1.getLiensByInjury(injList[0].Id);
        injList = Controller_Aura1.getInjuriesByAward(awList[0].Id);
        awList = Controller_Aura1.getAwardsByInjury(injList[0].Id);
        awList = Controller_Aura1.getAwardsByInjury(injList[0].Claimant__c);
        map<string,list<string>> metricMap = Controller_Aura1.getLawFirmMetrics(l[1].Action__c);
        list<Claimant__c> clList = Controller_Aura1.getLFClaimants(l[1].Action__c);
        map<string,list<Claimant__c>> clMap = Controller_Aura1.getOthers(l[1].Claimant__r.Law_Firm__c);
        map<string,integer> clStatusCount = Controller_Aura1.getClaimantStatusCount(l[1].Action__c);
        integer clTotal = Controller_Aura1.getClaimantsCountForAction(l[1].Action__c);
        clStatusCount = Controller_Aura1.getDistributionStatusCount(l[1].Action__c);
        clTotal = Controller_Aura1.getAwardsCountForAction(l[1].Action__c);
        map<string,map<string,map<string,string>>> stageMetrics = Controller_Aura1.getStageMetrics(l[1].Action__c);
        Release__c release = Controller_Aura1.getLastRelease(l[0].Claimant__c);
        release = Controller_Aura1.getLastRelease(l[1].Claimant__c);
        metricMap = Controller_Aura1.getLienMetrics(l[0].Action__c);
		list<string> rts = Controller_Aura1.getLienRecordTypes();
		map<string,string> testMap = Controller_Aura1.getFilterMap();
		string actionStr = Controller_Aura1.setAction('123456789012345678');
		system.assertEquals('Success', actionStr);
		ActionAssignment2__c actGone = ActionAssignment2__c.getInstance(userInfo.getUserId());
		delete actGone;
		actionStr = Controller_Aura1.setAction(l[0].Action__c);
        test.stopTest();
    }
}