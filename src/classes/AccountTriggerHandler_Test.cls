/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: PD-524/525/526/527 - Automation - Handler for Account trigger events - first pass - create Action_Accounts after insert.
			 Coverage as of 5/16 - 100%
*/
@isTest
private class AccountTriggerHandler_Test {

    static testMethod void TestActionAccountCreation() {
    	Id rtId1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
    	Id rtId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Lienholder').getRecordTypeId();
		Id pId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1].Id;
		User u = new User(UserName = 'TestUser@structures.com',
						  isActive = true,
						  ProfileId = pId,
						  LastName = 'User', 
						  Email = 'rap@rap.com', 
						  Alias = 'tuser', 
						  TimeZoneSidKey = 'America/Denver', 
						  LocaleSidKey = 'En_US', 
						  EmailEncodingKey = 'ISO-8859-1', 
						  LanguageLocaleKey = 'en_US');
		insert u;
        Action__c action1 = new Action__c(Name = 'Test Action 1',
                                          Active__c = true);
        Action__c action2 = new Action__c(Name = 'Test Action 2',
                                          Active__c = true);
        insert new list<Action__c>{action1,action2};
		test.startTest();	
		system.runAs(u) {
			ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
																Action__c = action1.Id);
			insert aac;
	        Account a = new Account(Name = 'Law Firm',
	        						RecordTypeId = rtId1,
	        						Priority_Firm__c = true, 
	                                BillingStreet = '123 Main St',
	                                BillingCity = 'Denver',
	                                BillingState = 'CO',
	                                BillingPostalCode = '80202');
	        Account b = new Account(Name = 'Lienholder',
	        						RecordTypeId = rtId2,
	                                BillingStreet = '345 Elm St',
	                                BillingCity = 'Portland',
	                                BillingState = 'OR',
	                                BillingPostalCode = '50303');
	        insert new list<Account>{a,b};
			aac.Action__c = action2.Id;
			update aac;
	        Account a1 = new Account(Name = 'Referring Law Firm',
	        						 RecordTypeId = rtId1,
	        						 Lead_Law_Firm__c = a.Id, 
	                                 BillingStreet = '124 Main St',
	                                 BillingCity = 'Denver',
	                                 BillingState = 'CO',
	                                 BillingPostalCode = '80202');
	        Account b1 = new Account(Name = 'LRP Provider',
	        						 RecordTypeId = rtId2,
	                                 BillingStreet = '124 Main St',
	                                 BillingCity = 'Denver',
	                                 BillingState = 'CO',
	                                 BillingPostalCode = '80202');
	        insert new list<Account>{a1,b1};
		}
		test.stopTest();
		list<Action_Account__c> checklist1 = [SELECT Id FROM Action_Account__c WHERE Action__c = :action1.Id];
		list<Action_Account__c> checklist2 = [SELECT Id FROM Action_Account__c WHERE Action__c = :action2.Id];
		system.assertEquals(2, checklist1.size());
		system.assertEquals(2, checklist2.size());
    }
}