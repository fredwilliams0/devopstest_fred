/*
    Created: lmsw - March 2017 for March release
    Purpose: Handler for Action trigger events
    		 Add method to create a box folder when a Claimant is created
History:

	Updated: RAP March 2017 for May Release
	Purpose: PRODSUPT-47 - Claimant Name field is inserted as record ID

	Updated: RAP April 2017 for May Release
	Purpose: techDebt - Operations Flow Control Prep

    Updated: lmsw - April 2017
    Purpose: PRODSUPT - 121 Modify to allow inserting a Claimant name.

	Updated: RAP May 2017 for May15 Release
	Purpose: PD-527 - Automation (rule #12)

    Updated: lmsw - May 2017
    Purpose: PD-656 Add new ActionClaimant when a new Claimant is added
	
	Updated: RAP May 2017 for May15 Release
	Purpose: PD-524/525/526/527 - automation - create appropriate liens on Claimant insert
								- automation - remove duplicate Claimants and create new ActionClaimant junction only
*/

public with sharing class ClaimantTriggerHandler {

	public static map<string, Claimant__c> ssnMap{get;set;}
	
    public void onAfterInsert(list<Claimant__c> newClaimantRecs, map<Id, Claimant__c> newClaimantsMap) {
    	set<Id> claimantIds = newClaimantsMap.keySet();
    	createBoxFolder(claimantIds);
    	CreateLiens(newClaimantRecs);
    }
// start techDebt         
    public void onBeforeUpdate(map<Id, Claimant__c> newMap, map<Id, Claimant__c> oldMap) {
		CalculateFormulaFields(newMap);
		CheckForSSNChange(newMap, oldMap); // PD-527
    } 
//end techDebt
// start PRODSUPT-47
    public void onBeforeInsert(list<Claimant__c> claimants) {
		NameClaimants(claimants);
    }
// end PRODSUPT-47
    public void onAfterUpdate(list<Claimant__c> newClaimantRecs, list<Claimant__c> oldClaimantRecs, map<Id, Claimant__c> newClaimantsMap, map<Id, Claimant__c> oldClaimantsMap) {

    }
// start techDebt    
    private static void CalculateFormulaFields(map<Id,Claimant__c> newMap) {
    	list<Lien__c> lienList = [SELECT Claimant__c, Total_Fees_Applied__c, Outstanding_Amount__c, Stages__c 
    							  FROM Lien__c 
    							  WHERE Claimant__c IN :newMap.keySet()];
    	map<Id,integer> lienMap = new map<Id,integer>();
    	map<Id,decimal> feeMap = new map<Id,decimal>();
    	map<Id,decimal> outMap = new map<Id,decimal>();
    	map<Id,integer> pendingMap = new map<Id,integer>();
    	decimal fee = 0.00;
    	decimal out = 0.00;
    	integer pending = 0;
    	integer total = 0;
		for (Id key : newMap.keySet()) {
   			feeMap.put(key, fee);
   			outMap.put(key, out);
   			lienMap.put(key, total);
    		pendingMap.put(key, pending);
		}
		for (Lien__c l : lienList) {
			fee = l.Total_Fees_Applied__c;
			out = l.Outstanding_Amount__c;
			pending = 1;
			total = 1;
   			fee += feeMap.get(l.Claimant__c);
   			feeMap.put(l.Claimant__c, fee);
   			out += outMap.get(l.Claimant__c);
   			outMap.put(l.Claimant__c, out);
   			total += lienMap.get(l.Claimant__c);
   			lienMap.put(l.Claimant__c, total);
    		if (l.Stages__c != 'Final') {
    			pending += pendingMap.get(l.Claimant__c);
    			pendingMap.put(l.Claimant__c, pending);
    		}
    	}
    	for (Id c : newMap.keySet()) {
    		Claimant__c cl = newMap.get(c);
    		cl.Lien_Amount_Left__c = outMap.get(c);
    		cl.Liens_Pending__c = pendingMap.get(c);
    		cl.Num_Liens__c = lienMap.get(c);
    		cl.Total_Applied__c = feeMap.get(c);
    	}
    }
// end techDebt
// start PD-527
	private static void CheckForSSNChange(map<Id, Claimant__c> newMap, map<Id, Claimant__c> oldMap) {
		for (Id cId : newMap.keySet()) {
			if (newMap.get(cId).SSN__c != oldMap.get(cId).SSN__c)
				newMap.get(cId).Date_SSN_Changed__c = system.today();
		}
	}
// end PD-527
// start PD-524/525/526/527 (automation - create appropriate liens on Claimant insert)
	private static void CreateLiens(list<Claimant__c> claimants) {
    	map<string,Id> rtMap = new map<string,Id>();
    	map<Id,Schema.RecordTypeInfo> tempMap = Schema.SObjectType.Lien__c.getRecordTypeInfosById();
		for (Id rtId : tempMap.keySet()) {
			rtMap.put(tempMap.get(rtId).getName(), rtId);
		}
		Action__c action = [SELECT Id, Medicaid_Eligibility_Verification__c, Medicare_Individual_Resolution__c,
								   Medicare_Eligibility_Verification__c, Medicare_Model__c, PLRP__c,
								   (SELECT Id, Account__c, PLRP_Type__c FROM Actions_Accounts__r WHERE LRP_Provider__c = true)
							FROM Action__c
							WHERE Id = :ActionAssignment2__c.getInstance(userInfo.getUserId()).Action__c limit 1];
		boolean medicaid = action.Medicaid_Eligibility_Verification__c;
		boolean medmodel = action.Medicare_Model__c;
		boolean medtrad = action.Medicare_Individual_Resolution__c;
		boolean plrp = action.PLRP__c;
		list<Account> medAccts = [SELECT Id, Name, BillingState FROM Account WHERE (Name = 'First Recovery Group' 
																				 OR Name = 'HMS'
																				 OR Name = 'Centers for Medicare & Medicaid Services'
																				 OR Name like '% - Medicaid')];
		map<string,Id> medicacctMap = new map<string,Id>();
		for (Account a : medAccts) {
			if (a.Name == 'First Recovery Group' || a.Name == 'HMS' || a.Name == 'Centers for Medicare & Medicaid Services')
				medicacctMap.put(a.Name, a.Id);
			else
				medicacctMap.put(a.BillingState, a.Id);
		}
		map<string,list<Model_Detail__c>> medicaidMap = new map<string,list<Model_Detail__c>>();
		map<string,list<Model_Detail__c>> plrpMap = new map<string,list<Model_Detail__c>>();
		map<Id,list<Model_Detail__c>> medicareMap = new map<Id,list<Model_Detail__c>>();
		list<Lien__c> insertList = new list<Lien__c>();
		list<Model_Detail__c> models = [SELECT Action__c, Medicaid_Aggregate__c, Medicare_Aggregate__c, Apply_Terms__c, Award_Type__c, 
											   Calculate_Final_Using__c, Cap_Holdback__c, Comments__c, Covered_Awards__c, Eligibility__c, 
											   FRG__c, HMS__c, Name, Payable_Amount__c, PayerType__c, Provider__c, Id, RecordType.Name, 
											   Reduction__c, State__c, State_Agency__c
										FROM Model_Detail__c
										WHERE Action__c = :action.Id];
		for (Model_Detail__c model : models) {
			if (model.RecordType.Name == 'Medicaid') {
				if (medicaidMap.containsKey(model.State__c))
					medicaidMap.get(model.State__c).add(model);
				else
					medicaidMap.put(model.State__c, new list<Model_Detail__c>{model});
			}
			else if (model.RecordType.Name == 'PLRP') {
				if (plrpMap.containsKey(Model.Provider__c))
					plrpMap.get(model.Provider__c).add(model);
				else
					plrpMap.put(model.Provider__c, new list<Model_Detail__c>{model});
			}
			else if (model.RecordType.Name == 'Medicare') {
				if (medicareMap.containsKey(Model.Provider__c))
					medicareMap.get(model.Provider__c).add(model);
				else
					medicareMap.put(model.Provider__c, new list<Model_Detail__c>{model});
			}
		}
		for (Claimant__c cc : claimants) {
			Claimant__c c = ssnMap.containsKey(cc.SSN__c) ? ssnMap.get(cc.SSN__c) : cc;
			if (medicaidMap.containsKey(c.State__c) && medicaid) {
				for (Model_Detail__c model : medicaidMap.get(c.State__c)) {
					if (model.FRG__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtMap.get('Medicaid'),
												Account__c = medicacctMap.get('First Recovery Group'),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					if (model.HMS__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtMap.get('Medicaid'),
												Account__c = medicacctMap.get('HMS'),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					if (model.State_Agency__c) {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtMap.get('Medicaid'),
												Account__c = medicacctMap.get(c.State__c),
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
				}
			}
			if (medmodel) {
				Lien__c l = new Lien__c(Claimant__c = c.Id,
										Action__c = action.Id,
										RecordTypeId = rtMap.get('Medicare Global Model'),
										Account__c = medicacctMap.get('Centers for Medicare & Medicaid Services'),
										Stages__c = 'To_Be_Submitted',
										State__c = c.State__c);
				insertList.add(l);
			}
			if (medtrad) {
				Lien__c l = new Lien__c(Claimant__c = c.Id,
										Action__c = action.Id,
										RecordTypeId = rtMap.get('Medicare Individual Resolution'),
										Account__c = medicacctMap.get('Centers for Medicare & Medicaid Services'),
										Stages__c = 'To_Be_Submitted');
				insertList.add(l);
			}
			if (plrp && c.Opted_in_PLRP__c == 'Yes') {
				for (Action_Account__c aac : action.Actions_Accounts__r) {
					if (aac.PLRP_Type__c == 'Claim_Detail') {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtMap.get('PLRP Claim Detail'),
												Account__c = aac.Account__c,
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
					else {
						Lien__c l = new Lien__c(Claimant__c = c.Id,
												Action__c = action.Id,
												RecordTypeId = rtMap.get('PLRP Model'),
												Account__c = aac.Account__c,
												Stages__c = 'To_Be_Submitted',
												State__c = c.State__c);
						insertList.add(l);
					}
				}
			}
		}
		if (!insertList.isEmpty())
			insert insertList;
	}
// end PD-524/525/526/527
@future (callout=true)
    private static void createBoxFolder(set<Id> cIds) {
        // Instantiate the Toolkit object
        box.Toolkit boxToolkit = new box.Toolkit();
        // Create a folder and associate it with an Action
        for (Id cId : cIds) {
        	string claimantFolderId = boxToolkit.createFolderForRecordId(cId,null,true);
        }
        if (!test.isRunningTest()) 
        	boxToolkit.commitChanges();
    }
// start PRODSUPT-47
    private static void NameClaimants(list<Claimant__c> claimants) {
		list<string> ssnList = new list<string>();
		for (Claimant__c c : claimants)
			ssnList.add(c.SSN__c);
		list<Claimant__c> dupes = [SELECT Id, State__c, SSN__c FROM Claimant__c WHERE SSN__c IN :ssnList];
		ssnMap = new map<string, Claimant__c>();
		for (Claimant__c c : dupes)
			ssnMap.put(c.SSN__c, c);
		Id qId = [SELECT Id FROM Group WHERE Type = 'Queue' AND Name = 'Opted Out of PLRP' limit 1].Id;
		for (Claimant__c c : claimants) {
            if(!string.isBlank(c.First_Name__c)){       // PRODSUPT-121
        		c.Name = c.First_Name__c;
        		if (!string.isBlank(c.Middle__c))
    				c.Name += ' ' +  c.Middle__c;
    			c.Name += ' ' + c.Last_Name__c;
            }
    	}
    }
// end PRODSUPT-47
}