/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries
			 Test Coverage as of 3/29: Injury trigger: 100%
			 						   InjuryUtility: 100%
*/
@isTest
private class InjuryUtility_Test {

@testSetup
	static void CreateData() {
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = true);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Claimant__c claim = new Claimant__c(Address_1__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
	}
	
    static testMethod void TestNaming() {
		Claimant__c c = [SELECT Id, Name FROM Claimant__c WHERE First_Name__c = 'RAP' limit 1];
		Id aId = [SELECT Id FROM Action__c WHERE Name = 'Test Action'].Id;
		Injury__c i1 = new Injury__c(Name = 'Injury1', 
									 Action__c = aId,
									 Claimant__c = c.Id,
									 Injury_Description__c = 'Broken Mandible',
                                     Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
    	test.startTest();
    	insert i1;
    	test.stopTest();
    	i1 = [SELECT Name FROM Injury__c WHERE Id = :i1.Id];
    	system.assertEquals('RAP RAPPER - Broken Mandible', i1.Name);
    }
}