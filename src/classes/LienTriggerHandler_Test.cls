/*
	Created: lmsw - March 2017 
	Purpose: test coverage for LienTriggerHandler class
	
	Updated: RAP - April 2017 for May Release
	Purpose: Fix failing testMethod
			 Coverage as of 4/25 - 85%
*/

@isTest
private class LienTriggerHandler_Test {
	
	static testMethod void testLienHoldBack() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Id rtId_lien = Schema.SObjectType.Lien__c.getRecordTypeInfosByName().get('PLRP Model').getRecordTypeId();
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = true);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Claimant__c c = new Claimant__c(Address_1__c = '123 Main St',
										City__c = 'Denver',
										Email__c = 'rap@rap.com',
										Law_Firm__c = acc.Id,
										First_Name__c = 'RAP',
										Last_Name__c = 'RAPPER',
										Phone__c = '3035551212',
										SSN__c = '135461448',
										State__c = 'CO',
										Status__c = 'Not_Ready_to_be_Cleared',
										Zip__c = '80138');
        insert c;
        Lien__c l = new Lien__c(Name__c = 'Test Lien',
        						RecordTypeId = rtId_lien,
        						Stages__c = 'To Be Submitted',
        						Claimant__c = c.Id,
        						Account__c = acc.Id,
        						Action__c = action.Id,
        						HBAmount2__c = 1000.00);
        insert l;
        system.debug('Lien: ' + l);
		Lien_Negotiated_Amounts__c lna = new Lien_Negotiated_Amounts__c(
									Lien__c = l.Id,
									Lien_Amount__c = 10000.00,
									Lien_Amount_Date__c = system.today(),
									Phase__c = 'Final');
		insert lna;
		test.startTest();
// test adding Holdback Percent
		l.HBPercent__c = 25;
		update l;
		system.assertEquals(25, [SELECT HBPercent__c FROM Lien__c WHERE Id =: l.Id].HBPercent__c);
		system.assertEquals(2500.00, [SELECT HBAmount2__c FROM Lien__c WHERE Id =: l.Id].HBAmount2__c);
			
// test adding Holdback Amount
		l.HBAmount2__c = 4500.00;
		update l;
		system.assertEquals(null, [SELECT HBPercent__c FROM Lien__c WHERE Id =: l.Id].HBPercent__c);
		system.assertEquals(4500.00, [SELECT HBAmount2__c FROM Lien__c WHERE Id =: l.Id].HBAmount2__c);
		test.stopTest();
	} 
}