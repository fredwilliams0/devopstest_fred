/*
    Created: RAP - March 2017 for May Release
    Purpose: PD-465 - Lien Detail Page - Notes, Tasks, and Phone calls
    
*/
trigger CDNoteTrigger on ContentDocument (after insert, after update, before delete) {

	if (trigger.isAfter) {
		if (trigger.isInsert)
			NoteUtility.onAfterInsert(trigger.newMap.keySet());
		else if (trigger.isUpdate)
			NoteUtility.onAfterUpdate(trigger.newMap.keySet(), trigger.oldMap);
	}
//    else if (trigger.isBefore && trigger.isDelete)
//        NoteUtility.onBeforeDelete(trigger.newMap.keySet());
}