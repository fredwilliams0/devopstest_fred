/*
	Created: RAP - December 2016 for first release
	Purpose: PD-319 - Create Trigger for Lien Insert

	Updated: RAP - January 2017 for first release
	Purpose: Add setting of recordtype, move processing to utility class 

	Updated: lmsw - March 2017
	Purpose: Modify to allow Holdback entry to be Percent or Amount

    Updated: RAP April 2017 for May Release
    Purpose: PD-566 - Automation - Create Lien Res Fees from Fee Model Object
    
    Updated: RAP May 2017 for May15 Release
    Purpose: PD-527 - Automation - calculate Final Payable
*/
trigger Lien on Lien__c (before insert, before update, after insert, after update) {
    
    LienTriggerHandler handler = new LienTriggerHandler();

    if (trigger.isBefore) {
   		if (trigger.isInsert) {
	   		LienUtility.BeforeInsert(trigger.new); 
   			handler.onBeforeInsert(trigger.new, trigger.newMap);
   		}
   		if (trigger.isUpdate) {
	   		LienUtility.BeforeUpdate(trigger.new, trigger.newMap, trigger.oldMap);
   			handler.onBeforeUpdate(trigger.new, trigger.newMap, trigger.oldMap);
   		}
    }
// start PD-566
    if (trigger.isAfter) {
    	if (trigger.isInsert)
    		LienUtility.AfterInsert(trigger.new, trigger.newMap);
    	if (trigger.isUpdate)
			LienUtility.AfterUpdate(trigger.new, trigger.newMap, trigger.oldMap);
    }
// end PD-566
}