/*
	Created: RAP - March 2017 for May Release
	Purpose: PRODSUPT-53 - Add automatic naming for Injuries

*/
trigger Injury on Injury__c (before insert) {
    
	InjuryUtility.NameInjuries(trigger.new);
	
}