/*
    Created: RAP - May 2017 for May22 Patch
    Purpose: Receive trigger events and pass the handling of those events to the AccountTriggerHandler class
*/

trigger Account on Account (after insert, after update) {

AccountTriggerHandler handler = new AccountTriggerHandler();
    
//    if (trigger.isBefore && trigger.isInsert) {
//        handler.onBeforeInsert(trigger.new, trigger.newMap);
//    }
    
//    if(trigger.isBefore && trigger.isUpdate) {
//        handler.onBeforeUpdate(trigger.new, trigger.newMap, trigger.oldMap);
//    }
    
    if (trigger.isAfter && trigger.isInsert) {
        handler.onAfterInsert(trigger.new, trigger.newMap);
    }
    
    if (trigger.isAfter && trigger.isUpdate) {
        handler.onAfterUpdate(trigger.new, trigger.oldMap);
    }
}