/*
    Created: MEF - January 2017 for first release
    Purpose: Receive trigger events and pass the handling of those events
             to the ClaimantTriggerHandler class
History:

	Updated: RAP - March 2017 for May Release
	Purpose: PRODSUPT-47 - Claimant Name field is inserted as record ID

	Updated: RAP April 2017 for May Release
	Purpose: techDebt - Operations Flow Control Prep
	
*/

trigger Claimant on Claimant__c (before insert, before update, after insert, after update, before delete) { // techDebt (added before update)

	ClaimantTriggerHandler handler = new ClaimantTriggerHandler();
	    
    if (trigger.isBefore && trigger.isInsert)
        handler.onBeforeInsert(trigger.new);
    if (trigger.isBefore && trigger.isUpdate)
        handler.onBeforeUpdate(trigger.newMap, trigger.oldMap);
    if (trigger.isAfter && trigger.isInsert)
        handler.onAfterInsert(trigger.new, trigger.newMap);
    if (trigger.isAfter && trigger.isUpdate)
        handler.onAfterUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
}